create schema social_network_system collate latin1_swedish_ci;

use social_network_system;
create table movie_genres
(
    genre_id int auto_increment
        primary key,
    genre varchar(50) not null
);

create table movies
(
    movie_id int auto_increment
        primary key,
    title varchar(150) not null,
    imdb varchar(50) not null,
    poster varchar(255) not null
);

create table users_address
(
    address_id int auto_increment
        primary key,
    town varchar(50) null,
    country varchar(100) null,
    privacy tinyint(1) not null
);

create table users_work
(
    work_id int auto_increment
        primary key,
    company varchar(50) null,
    position varchar(50) null,
    from_date varchar(10) null,
    to_date varchar(10) null,
    descr varchar(500) null,
    privacy tinyint(1) not null,
    enabled tinyint(1) not null
);

create table users_info
(
    user_id int auto_increment
        primary key,
    first_name varchar(50) null,
    last_name varchar(50) null,
    email varchar(100) null,
    email_privacy tinyint(1) not null,
    picture mediumtext null,
    picture_privacy tinyint not null,
    cover_picture mediumtext null,
    gender varchar(6) null,
    gender_privacy tinyint not null,
    birthday varchar(12) null,
    birthday_privacy tinyint not null,
    work_id int null,
    address_id int null,
    description varchar(500) null,
    constraint users_info_users_address_address_id_fk
        foreign key (address_id) references users_address (address_id),
    constraint users_info_users_work_work_id_fk
        foreign key (work_id) references users_work (work_id)
);

create table tokens_users
(
    token_id int auto_increment
        primary key,
    confirmation_token varchar(255) null,
    created_date datetime null,
    user_id int null,
    constraint tokens_users_users_info_user_id_fk
        foreign key (user_id) references users_info (user_id)
);

create table users
(
    username varchar(50) not null
        primary key,
    password varchar(68) not null,
    enabled tinyint not null,
    user_id int null,
    constraint users_users_info_user_id_fk
        foreign key (user_id) references users_info (user_id)
);

create table authorities
(
    username varchar(50) not null,
    authority varchar(50) not null,
    constraint username_authority
        unique (username, authority),
    constraint authorities_fk
        foreign key (username) references users (username)
);

create table users_friends
(
    id int auto_increment
        primary key,
    user_id int not null,
    friend_id int not null,
    status varchar(10) not null,
    constraint users_friends_users_info_user_id_fk
        foreign key (user_id) references users_info (user_id),
    constraint users_friends_users_info_user_id_fk_2
        foreign key (friend_id) references users_info (user_id)
);

create table users_genres
(
    user_id int not null,
    genre_id int not null,
    constraint users_genres_movie_genres_genre_id_fk
        foreign key (genre_id) references movie_genres (genre_id),
    constraint users_genres_users_info_user_id_fk
        foreign key (user_id) references users_info (user_id)
);

create table users_movies
(
    user_id int not null,
    movie_id int not null,
    constraint users_movies_movies_movie_id_fk
        foreign key (movie_id) references movies (movie_id),
    constraint users_movies_users_info_user_id_fk
        foreign key (user_id) references users_info (user_id)
);

create table users_posts
(
    post_id int auto_increment
        primary key,
    text varchar(1500) null,
    picture mediumtext null,
    user_id int not null,
    privacy tinyint not null,
    date_and_time timestamp default current_timestamp() not null on update current_timestamp(),
    constraint users_posts_users_info_user_id_fk
        foreign key (user_id) references users_info (user_id)
);

create table posts_comments
(
    comment_id int auto_increment
        primary key,
    text varchar(500) not null,
    post_id int not null,
    date_and_time timestamp default current_timestamp() not null on update current_timestamp(),
    user_id int not null,
    constraint FK_posts_comments_users_info
        foreign key (user_id) references users_info (user_id),
    constraint posts_comments_users_posts_post_id_fk
        foreign key (post_id) references users_posts (post_id)
);

create table posts_likes
(
    like_id int auto_increment
        primary key,
    post_id int not null,
    user_id int not null,
    status varchar(255) null,
    constraint posts_likes_users_info_user_id_fk
        foreign key (user_id) references users_info (user_id),
    constraint posts_likes_users_posts_post_id_fk
        foreign key (post_id) references users_posts (post_id)
);

create table users_notifications
(
    notification_id int auto_increment
        primary key,
    user_id int not null,
    text varchar(500) not null,
    date_and_time timestamp default current_timestamp() not null on update current_timestamp(),
    is_read tinyint(1) not null,
    post_id int not null,
    constraint users_notifications_users_info_user_id_fk
        foreign key (user_id) references users_info (user_id),
    constraint users_notifications_users_posts_post_id_fk
        foreign key (post_id) references users_posts (post_id)
);

