package com.team5.socialnetwork;

import com.team5.socialnetwork.models.Comments;
import com.team5.socialnetwork.models.Posts;
import com.team5.socialnetwork.models.UserInfo;

public class CommentsTestFactory {

    public static Comments createComment() {
        UserInfo user = new UserInfo(1);
        Posts post = new Posts("text", user);
        Comments comment = new Comments("testComment", post, user);
        return comment;
    }
}
