package com.team5.socialnetwork.services;

import com.team5.socialnetwork.models.MovieGenre;
import com.team5.socialnetwork.repositories.MovieGenresRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyString;

@RunWith(MockitoJUnitRunner.class)
public class MovieGenresServiceImplTests {

    @Mock
    MovieGenresRepository repository;

    @InjectMocks
    MovieGenresServiceImpl mockService;

    @Test
    public void getAllShould_ReturnAllMovieGenres() {
        //Arrange
        List<MovieGenre> expected = new ArrayList<>();
        //Act
        Mockito.when(repository.findAll()).thenReturn(expected);
        List<MovieGenre> returned = mockService.getAll();
        //Assert
        Assert.assertSame(expected, returned);
    }

    @Test
    public void getByNameShould_ReturnMovieGenreByName() {
        //Arrange
        MovieGenre expected = new MovieGenre();
        //Act
        Mockito.when(repository.findByGenre(anyString())).thenReturn(expected);
        MovieGenre returned = mockService.getByName(anyString());
        //Assert
        Assert.assertSame(expected, returned);
    }
}
