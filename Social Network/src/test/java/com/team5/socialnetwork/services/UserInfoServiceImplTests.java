package com.team5.socialnetwork.services;

import com.team5.socialnetwork.models.*;
import com.team5.socialnetwork.repositories.FriendsRepository;
import com.team5.socialnetwork.repositories.UsersInfoRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.team5.socialnetwork.CommentsTestFactory.createComment;
import static com.team5.socialnetwork.PostsTestFactory.createCreator;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class UserInfoServiceImplTests {

    @Mock
    UsersInfoRepository repository;

    @Mock
    FriendsRepository friendsRepository;

    @InjectMocks
    UsersInfoServiceImpl mockService;

    @Test
    public void getAllByNameShould_ReturnAllUsersByPassedFirstName() {
        //Arrange
        User user = new User();
        user.setEnabled(true);
        UserInfo expected = new UserInfo();
        expected.setUser(user);
        List<UserInfo> userList = new ArrayList<>();
        userList.add(expected);
        //Act
        Mockito.when(repository.findAllByFirstNameLike(anyString())).thenReturn(userList);
        Map<Integer, UserInfo> usersMap = mockService.getAllByName(anyString());
        UserInfo returned = usersMap.get(0);
        //Assert
        Assert.assertSame(expected, returned);
    }

    @Test
    public void getAllByNameShould_ReturnAllUsersByPassedLastName() {
        //Arrange
        User user = new User();
        user.setEnabled(true);
        UserInfo expected = new UserInfo();
        expected.setUser(user);
        List<UserInfo> userList = new ArrayList<>();
        userList.add(expected);
        //Act
        Mockito.when(repository.findAllByLastNameLike(anyString())).thenReturn(userList);
        Map<Integer, UserInfo> usersMap = mockService.getAllByName(anyString());
        UserInfo returned = usersMap.get(0);
        //Assert
        Assert.assertSame(expected, returned);
    }

    @Test
    public void getAllByNameAndStatusFriendsShould_ReturnAllUsersByPassedName() {
        //Arrange
        User user = new User();
        user.setEnabled(true);
        UserInfo expected = new UserInfo();
        expected.setId(1);
        expected.setUser(user);

        User user1 = new User();
        user1.setEnabled(true);
        UserInfo expected1 = new UserInfo();
        expected1.setId(1);
        expected1.setUser(user1);

        List<UserInfo> userList = new ArrayList<>();
        userList.add(expected);

        Friends friends = new Friends(expected, expected1, "friends");

        //Act
        Mockito.when(repository.findAllByLastNameLike(anyString())).thenReturn(userList);
        Map<Integer, UserInfo> usersMap = mockService.getAllByName(anyString());
        UserInfo returned = usersMap.get(1);
        Mockito.when(friendsRepository.findByRequestReceiverAndRequestSenderAndStatus(expected, returned, "friends")).thenReturn(friends);
        List<UserInfo> list = mockService.getAllByNameAndStatusFriends(expected, anyString());
        UserInfo result = list.get(0);
        //Assert
        Assert.assertSame(expected, result);
    }

    @Test
    public void getAllByNameAndOtherStatusShould_ReturnAllUsersByPassedName() {
        //Arrange
        User user = new User();
        user.setEnabled(true);
        UserInfo expected = new UserInfo();
        expected.setId(1);
        expected.setUser(user);

        User user1 = new User();
        user1.setEnabled(true);
        UserInfo expected1 = new UserInfo();
        expected1.setId(1);
        expected1.setUser(user1);

        List<UserInfo> userList = new ArrayList<>();
        userList.add(expected);

        Friends friends = new Friends(expected, expected1, "request");

        //Act
        Mockito.when(repository.findAllByLastNameLike(anyString())).thenReturn(userList);
//      Map<Integer, UserInfo> usersMap = mockService.getAllByName(anyString());
//      UserInfo returned = usersMap.get(1);
//      Mockito.when(friendsRepository.findByRequestReceiverAndRequestSenderAndStatus(expected, returned, "request")).thenReturn(friends);
        List<UserInfo> list = mockService.getAllByNameAndOtherStatus(expected, anyString());
        UserInfo result = list.get(0);
        //Assert
        Assert.assertSame(expected, result);
    }

    @Test
    public void getByUserIdShould_ReturnUserInfoById() {
        //Arrange
        User user = new User();
        user.setEnabled(true);
        UserInfo expected = new UserInfo();
        expected.setUser(user);
        //Act
        Mockito.when(repository.findById(anyInt())).thenReturn(expected);
        UserInfo returned = mockService.getByUserId(anyInt());
        //Assert
        Assert.assertSame(expected, returned);
    }

}
