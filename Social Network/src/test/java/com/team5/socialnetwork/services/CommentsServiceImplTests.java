package com.team5.socialnetwork.services;

import com.team5.socialnetwork.models.Comments;
import com.team5.socialnetwork.models.Posts;
import com.team5.socialnetwork.models.UserInfo;
import com.team5.socialnetwork.repositories.CommentsRepository;
import com.team5.socialnetwork.repositories.PostsRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import javax.persistence.EntityNotFoundException;

import java.util.ArrayList;
import java.util.List;

import static com.team5.socialnetwork.CommentsTestFactory.createComment;
import static com.team5.socialnetwork.PostsTestFactory.createCreator;
import static com.team5.socialnetwork.PostsTestFactory.createPost;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CommentsServiceImplTests {

    @Mock
    CommentsRepository repository;

    @InjectMocks
    CommentsServiceImpl mockService;

    @Test
    public void saveCommentShould_CreateComment() {
        //Arrange
        Comments comment = createComment();
        //Act
        mockService.saveComment(comment);
        //Assert
        verify(repository, times(1)).save(comment);
    }

    @Test
    public void editCommentShould_EditComment() {
        //Arrange
        Comments comment = createComment();
        //Act
        mockService.editComment(comment);
        //Assert
        verify(repository, times(1)).save(comment);
    }

    @Test
    public void findByIdShould_ReturnCommentById() {
        //Arrange
        Comments expected = createComment();
        //Act
        Mockito.when(repository.findById(anyLong())).thenReturn(expected);
        Comments returned = mockService.findById(1);
        //Assert
        Assert.assertSame(expected, returned);
    }

    @Test
    public void findByIdShouldThrow_WhenPassedInvalidComment() {
        //Arrange
        //Act
        Mockito.when(repository.findById(anyLong())).thenReturn(null);
        //Assert
        Assertions.assertThrows(EntityNotFoundException.class, () -> mockService.findById(1));
    }

    @Test
    public void findByPostShould_ReturnCommentByPost() {
        //Arrange
        UserInfo user = createCreator();
        Posts post = createPost();
        Comments comment = new Comments("testComment", post, user);
        List<Comments> expected = new ArrayList<>();
        expected.add(comment);
        //Act
        Mockito.when(repository.findByPost(post)).thenReturn(expected);
        List<Comments> returned = mockService.findByPost(post);
        //Assert
        Assert.assertSame(expected, returned);
    }

    @Test
    public void deleteAllCommentsByPostShould_DeleteComments() {
        //Arrange
        UserInfo user = createCreator();
        Posts post = createPost();
        Comments comment = new Comments("testComment", post, user);
        List<Comments> expected = new ArrayList<>();
        expected.add(comment);
        //Act
        mockService.deleteCommentsByPost(post);
        //Assert
        verify(repository, times(1)).deleteAllByPost(post);
    }

    @Test
    public void deleteAllCommentsByUserShould_DeleteComments() {
        //Arrange
        UserInfo user = createCreator();
        Posts post = createPost();
        Comments comment = new Comments("testComment", post, user);
        List<Comments> expected = new ArrayList<>();
        expected.add(comment);
        //Act
        mockService.deleteCommentsByUser(user);
        //Assert
        verify(repository, times(1)).deleteAllByCreator(user);
    }

}
