package com.team5.socialnetwork.services;

import com.google.common.util.concurrent.ListeningExecutorService;
import com.team5.socialnetwork.models.Likes;
import com.team5.socialnetwork.models.Posts;
import com.team5.socialnetwork.models.UserInfo;
import com.team5.socialnetwork.repositories.LikesRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class LikesServiceImplTests {

    @Mock
    LikesRepository repository;

    @InjectMocks
    LikesServiceImpl mockService;

    @Test
    public void thumbOnPostShould_CreateLikeWhenPassedNewValue() {
        //Arrange
        Likes thumb = new Likes();
        //Act
        mockService.thumbOnPost(thumb);
        //Assert
        verify(repository, times(1)).save(thumb);
    }

    @Test
    public void thumbOnPostShould_RemoveThumbFromPostWhenClickedSecondTime() {
        //Arrange
        UserInfo user = new UserInfo();
        Posts post= new Posts();
        Likes thumb = new Likes(post, user, "liked");
        Likes secondThumb = new Likes(post, user, "liked");
        //Act
        Mockito.when(repository.findByUserAndPost(user, post)).thenReturn(thumb);
        mockService.thumbOnPost(secondThumb);
        //Assert
        verify(repository, times(1)).delete(thumb);
    }

    @Test
    public void thumbOnPostShould_ChangeThumbStatusWhenClickedTheOpposite() {
        //Arrange
        UserInfo user = new UserInfo();
        Posts post= new Posts();
        Likes thumb = new Likes(post, user, "liked");
        Likes secondThumb = new Likes(post, user, "disliked");
        //Act
        Mockito.when(repository.findByUserAndPost(user, post)).thenReturn(thumb);
        mockService.thumbOnPost(secondThumb);
        //Assert
        verify(repository, times(1)).save(thumb);
    }

    @Test
    public void getPostsLikesShould_ReturnAllPostsLikes() {
        //Arrange
        UserInfo userOne = new UserInfo();
        UserInfo userTwo = new UserInfo();
        Posts post = new Posts();
        Likes firstLike = new Likes(post, userOne, "liked");
        Likes secondLike = new Likes(post, userTwo, "liked");
        List<Posts> postsList = new ArrayList<>();
        postsList.add(post);
        Page<Posts> expected = new PageImpl<>(postsList);
        //Act
        Mockito.when(repository.countAllByPostAndStatus(post, "liked")).thenReturn(2);
        Map<Posts, Integer> postsMap = mockService.getPostsLikes(expected);
        int returned = postsMap.get(post);
        //Assert
        Assert.assertEquals(2, returned);
    }

    @Test
    public void getPostsLikesShould_ReturnAllPostsDislikes() {
        //Arrange
        UserInfo userOne = new UserInfo();
        UserInfo userTwo = new UserInfo();
        Posts post = new Posts();
        Likes firstLike = new Likes(post, userOne, "disliked");
        Likes secondLike = new Likes(post, userTwo, "disliked");
        List<Posts> postsList = new ArrayList<>();
        postsList.add(post);
        Page<Posts> expected = new PageImpl<>(postsList);
        //Act
        Mockito.when(repository.countAllByPostAndStatus(post, "disliked")).thenReturn(2);
        Map<Posts, Integer> postsMap = mockService.getPostsDislikes(expected);
        int returned = postsMap.get(post);
        //Assert
        Assert.assertEquals(2, returned);
    }

    @Test
    public void deleteLikesByPostShould_DeleteThumbsWhenPassedPost() {
        //Arrange
        Posts post = new Posts();
        //Act
        mockService.deleteLikesByPost(post);
        //Assert
        verify(repository, times(1)).deleteAllByPost(post);
    }

    @Test
    public void deleteLikesByUserShould_DeleteThumbsWhenPassedUser() {
        //Arrange
        UserInfo user = new UserInfo();
        //Act
        mockService.deleteLikesByUser(user);
        //Assert
        verify(repository, times(1)).deleteAllByUser(user);
    }

    @Test
    public void findLikesByPostIdShould_ReturnLikesByPostId() {
        //Arrange
        Posts post = new Posts();
        Likes expected = new Likes();
        //Act
        Mockito.when(repository.findByPost(0)).thenReturn(expected);
        Likes returned = mockService.findByPostId(0);
        //Assert
        Assert.assertSame(expected, returned);
    }
}
