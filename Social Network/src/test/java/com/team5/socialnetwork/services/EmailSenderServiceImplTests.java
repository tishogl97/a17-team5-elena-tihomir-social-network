package com.team5.socialnetwork.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class EmailSenderServiceImplTests {

    @Mock
    JavaMailSender mailSender;

    @InjectMocks
    EmailSenderServiceImpl mockService;

    @Test
    public void sendEmailShould_SendEmail() {
        //Arrange
        SimpleMailMessage message = new SimpleMailMessage();
        //Act
        mockService.sendEmail(message);
        //Assert
        verify(mailSender, times(1)).send(message);
    }
}
