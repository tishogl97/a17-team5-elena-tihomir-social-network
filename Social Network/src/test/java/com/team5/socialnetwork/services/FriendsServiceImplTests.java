package com.team5.socialnetwork.services;

import com.team5.socialnetwork.exceptions.AlreadyConnectedException;
import com.team5.socialnetwork.models.Friends;
import com.team5.socialnetwork.models.User;
import com.team5.socialnetwork.models.UserInfo;
import com.team5.socialnetwork.repositories.FriendsRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class FriendsServiceImplTests {

    @Mock
    FriendsRepository repository;

    @InjectMocks
    FriendsServiceImpl mockService;

    @Test
    public void sendFriendRequestShould_SendFriendRequest() {
        //Arrange
        UserInfo sender = new UserInfo();
        UserInfo receiver = new UserInfo();
        Friends request = new Friends(sender, receiver, "request");
        //Act
        mockService.sendFriendRequest(request);
        //Assert
        verify(repository, times(1)).save(request);
    }

    @Test
    public void sendFriendRequestShouldThrow_WhenUsersAreAlreadyConnected() {
        //Arrange
        UserInfo sender = new UserInfo(1);
        UserInfo receiver = new UserInfo(2);
        Friends request = new Friends(sender, receiver, "friends");
        Mockito.when(repository.findByRequestReceiverAndRequestSender(sender, receiver)).thenReturn(request);
        //Act
        Assertions.assertThrows(AlreadyConnectedException.class, () -> mockService.sendFriendRequest(request));
        //Assert
    }

    @Test
    public void getAllRequestShould_ShowOnlyRequests() {
        //Arrange
        UserInfo receiver = new UserInfo();
        Friends request = new Friends();
        List<Friends> expected = new ArrayList<>();
        expected.add(request);
        when(repository.findAllByRequestReceiverAndStatus(receiver, "request")).thenReturn(expected);
        //Act
        List<Friends> result = mockService.getAllRequests(receiver);
        //Assert
        Assert.assertSame(expected, result);
    }

    @Test
    public void getAllRequestsWithAllStatsTypesShould_ReturnAllConnections() {
        //Arrange
        UserInfo receiver = new UserInfo();
        UserInfo sender = new UserInfo();
        UserInfo secondSender = new UserInfo();
        Friends friends = new Friends(sender, receiver, "friends");
        Friends request = new Friends(secondSender, receiver, "request");
        List<Friends> expected = new ArrayList<>();
        expected.add(friends);
        expected.add(request);
        //Act
        Mockito.when(repository.findByRequestReceiver(receiver.getId())).thenReturn(expected);
        List<Friends> result = mockService.getAllRequestsWithAllStatusTypes(receiver);
        //Assert
        Assert.assertSame(expected, result);

    }

    @Test
    public void getAllFriendsShould_ReturnAllFriends() {
        //Arrange
        UserInfo receiver = new UserInfo(1);
        UserInfo sender = new UserInfo(2);
        UserInfo secondSender = new UserInfo(3);
        Friends friends = new Friends(sender, receiver,  "friends");
        Friends request = new Friends(secondSender, receiver,  "friends");
        List<Friends> friendsList = new ArrayList<>();
        friendsList.add(friends);
        friendsList.add(request);
        //Act
        Mockito.when(repository.findAllByRequestReceiverAndStatus(receiver, "friends")).thenReturn(friendsList);
        List<UserInfo> expected = new ArrayList<>();
        expected.add(friendsList.get(0).getRequestSender());
        expected.add(friendsList.get(1).getRequestSender());
        List<UserInfo> result = mockService.getAllFriends(receiver);
        //Assert
        Assert.assertEquals(expected, result);
    }

    @Test
    public void findByIdShould_ReturnConnectionById() {
        //Arrange
        Friends expected = new Friends();
        //Act
        Mockito.when(repository.findById(anyLong())).thenReturn(expected);
        Friends returned = mockService.findById(1);
        //Assert
        Assert.assertSame(expected, returned);

    }

    @Test
    public void acceptRequestShould_AcceptRequests() {
        //Arrange
        Friends friends = new Friends();
        //Act
        mockService.acceptRequest(friends);
        //Assert
        verify(repository, times(1)).save(friends);
    }

    @Test
    public void declineRequestShould_DeleteTheRequest() {
        //Arrange
        Friends friends = new Friends();
        //Act
        mockService.declineRequest(friends);
        //Assert
        verify(repository, times(1)).delete(friends);
    }

    @Test
    public void removeFriendsShould_DeleteFriendsConnection() {
        //Arrange
        UserInfo firstUser = new UserInfo();
        UserInfo secondUser = new UserInfo();
        Friends friends = new Friends(firstUser, secondUser, "friends");
        //Act
        Mockito.when(repository.findByRequestReceiverAndRequestSender(firstUser, secondUser))
                .thenReturn(friends);
       mockService.removeFriend(firstUser, secondUser);
        //Assert
        verify(repository, times(1)).delete(friends);
    }

    @Test
    public void removeAllFriendsShould_ReturnFriendsCount() {
        //Arrange
        UserInfo firstUser = new UserInfo();
        UserInfo secondUser = new UserInfo();
        Friends friends = new Friends(firstUser, secondUser, "friends");
        List<Friends> receiver = new ArrayList<>();
        receiver.add(friends);
        //Act
        Mockito.when(repository.findByRequestReceiver(firstUser.getId()))
                .thenReturn(receiver);
        mockService.removeAllFriends(firstUser);
        //Assert
        verify(repository, times(receiver.size())).delete(friends);
    }

    @Test
    public void getFriendsCountShould_DeleteFriendsConnection() {
        //Arrange
        UserInfo firstUser = new UserInfo();
        UserInfo secondUser = new UserInfo();
        Friends friends = new Friends(firstUser, secondUser, "friends");
        List<Friends> expected = new ArrayList<>();
        expected.add(friends);
        //Act
        Mockito.when(repository.getCount()).thenReturn(expected.size());
        int returned =  mockService.getFriendsCount();
        //Assert
        Assert.assertEquals(1, returned);
    }

}
