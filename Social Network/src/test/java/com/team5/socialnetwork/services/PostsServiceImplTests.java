package com.team5.socialnetwork.services;

import com.team5.socialnetwork.exceptions.NoPermissionException;
import com.team5.socialnetwork.models.Posts;
import com.team5.socialnetwork.models.UserInfo;
import com.team5.socialnetwork.repositories.PostsRepository;


import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.configuration.injection.MockInjection;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;


import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

import static com.team5.socialnetwork.PostsTestFactory.createCreator;
import static com.team5.socialnetwork.PostsTestFactory.createPost;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class PostsServiceImplTests {

    @Mock
    PostsRepository repository;

    @InjectMocks
    PostsServiceImpl mockService;

    @Test
    public void savePostShould_CreatePostWhenPassedValid() {
        //Arrange
        Posts post = createPost();
        //Act
        mockService.savePost(post);
        //Assert
        verify(repository, times(1)).save(post);
    }

    @Test
    public void deletePostShould_DeletePostWhenPassedValidArguments() {
        //Arrange
        UserInfo user = createCreator();
        Posts post = new Posts("text", user);
        boolean isAdmin = false;
        //Act
        mockService.deletePost(post, user, isAdmin);
        //Assert
        verify(repository, times(1)).delete(post);
    }

    @Test
    public void deletePostsByUserShould_DeletePostWhenPassedValidArguments() {
        //Arrange
        UserInfo user = createCreator();
        Posts post = new Posts("text", user);
        //Act
        mockService.deletePostsByUser(user);
        //Assert
        verify(repository, times(1)).deleteAllByCreator(user);
    }

    @Test
    public void deletePostShouldThrow_WhenUserIsNotCreator() {
        //Arrange, Act
        UserInfo wrongUser = createCreator();
        Posts post = createPost();
        boolean isAdmin = false;

        //Assert
        Assertions.assertThrows(NoPermissionException.class, () -> mockService.deletePost(post, wrongUser, isAdmin));
    }

    @Test
    public void editPostShould_EditPostWhenPassedValidArguments() {
        //Arrange
        UserInfo user = createCreator();
        Posts post = new Posts("text", user);
        boolean isAdmin = false;
        //Act
        mockService.editPost(post, user, isAdmin);
        //Assert
        verify(repository, times(1)).save(post);
    }

    @Test
    public void editPostShouldThrow_WhenUserIsNotCreator() {
        //Arrange
        UserInfo wrongUser = createCreator();
        Posts post = createPost();
        boolean isAdmin = false;
        //Act
        //Assert
        Assertions.assertThrows(NoPermissionException.class, () -> mockService.editPost(post, wrongUser, isAdmin));
    }

    @Test
    public void showPersonalFeedShould_ReturnAllPersonalPosts() {
        //Arrange
        UserInfo user = createCreator();
        Posts post = new Posts("test", user);
        List<Posts> postsList = new ArrayList<>();
        postsList.add(post);
        Page<Posts> expected = new PageImpl<>(postsList);
        //Act
        Mockito.when(repository.findAllByCreatorOrderByDateDesc(user, PageRequest.of(0, 10))).thenReturn(expected);
        Page<Posts> returned = mockService.showPersonalFeed(user, PageRequest.of(0, 10));
        //Assert
        Assert.assertSame(expected, returned);
    }

    @Test
    public void findByIdShould_ReturnPostsByIdWhenPassedValidId() {
        //Arrange
        Posts expected = createPost();
        //Act
        Mockito.when(repository.findById(anyLong())).thenReturn(expected);
        Posts returned = mockService.findById(1);
        //Assert
        Assert.assertSame(expected, returned);
    }

    @Test
    public void findByIdShouldThrow_WhenPassedInvalidId() {
        //Arrange
        //Act
        Mockito.when(repository.findById(anyLong())).thenReturn(null);
        //Assert
        Assertions.assertThrows(EntityNotFoundException.class, () -> mockService.findById(1));
    }

    @Test
    public void getAllByLoggedUserShould_ReturnAllFriendsPosts() {
        //Arrange
        UserInfo loggedUser = createCreator();
        Posts post = createPost();
        List<Posts> expected = new ArrayList<>();
        expected.add(post);
        Page<Posts> postsPage = new PageImpl<>(expected);
        //Act
        Mockito.when(repository.getAllByLoggedUser(loggedUser, PageRequest.of(0, 10))).thenReturn(postsPage);
        Page<Posts> returned = mockService.getAllByLoggedUser(loggedUser, PageRequest.of(0, 10));
        //Assert
        Assert.assertSame(postsPage, returned);
    }

    @Test
    public void getTenPublicShould_ReturnFirstTenPosts() {
        //Arrange
        List<Posts> expected = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Posts post = new Posts();
            expected.add(post);
        }
        //Act
        Mockito.when(repository.getTenPublic()).thenReturn(expected);
        List<Posts> returned = mockService.getTenPublic();
        //Assert
        Assert.assertSame(expected, returned);
    }

    @Test
    public void getPostCountShould_ReturnPostCount() {
        //Arrange
        Posts post = new Posts();
        List<Posts> expected = new ArrayList<>();
        expected.add(post);
        //Act
        Mockito.when(repository.getCount()).thenReturn(expected.size());
        int returned = mockService.getPostCount();
        //Assert
        Assert.assertSame(expected.size(), returned);
    }
}
