package com.team5.socialnetwork.services;

import com.team5.socialnetwork.models.Movie;
import com.team5.socialnetwork.models.Posts;
import com.team5.socialnetwork.models.UserInfo;
import com.team5.socialnetwork.repositories.MoviesRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static com.team5.socialnetwork.PostsTestFactory.createPost;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class MoviesServiceImplTests {

    @Mock
    MoviesRepository repository;

    @InjectMocks
    MoviesServiceImpl mockService;

    @Test
    public void createMovieShould_SaveMovieWhenPassedValid() {
        //Arrange
        Movie movie = new Movie();
        //Act
        mockService.createMovie(movie);
        //Assert
        verify(repository, times(1)).save(movie);
    }

    @Test
    public void getMovieByTitleShould_ReturnMovieByTitleWhenPassedValidTitle() {
        //Arrange
        Movie expected = new Movie();
        expected.setTitle("test");
        //Act
        Mockito.when(repository.findByTitle("test")).thenReturn(expected);
        Movie returned = mockService.getMovieByTitle("test");
        //Assert
        Assert.assertSame(expected, returned);
    }

    @Test
    public void getAllMoviesByUserShould_ReturnMoviesPassedValidUserId() {
        //Arrange
        UserInfo user = new UserInfo();
        user.setId(1);
        Movie movie1 = new Movie();
        Movie movie2 = new Movie();
        List<Movie> expected = new ArrayList<>();
        expected.add(movie1);
        expected.add(movie2);
        //Act
        Mockito.when(repository.findAllByUser(1)).thenReturn(expected);
        List<Movie> returned = mockService.getAllMoviesByUser(1);
        //Assert
        Assert.assertSame(expected, returned);
    }

    @Test
    public void removeFromFavoritesShould_DeleteRowWhenPassedValidArguments() {
        //Arrange
        UserInfo user = new UserInfo();
        user.setId(1);
        Movie movie = new Movie();
        movie.setId(1);
        //Act
        mockService.removeFromFavorites(1,1);
        //Assert
        verify(repository, times(1)).removeFromFavorites(1,1);
    }

}
