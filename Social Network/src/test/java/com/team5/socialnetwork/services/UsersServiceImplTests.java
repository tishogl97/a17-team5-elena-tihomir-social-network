package com.team5.socialnetwork.services;

import com.team5.socialnetwork.exceptions.DuplicateEntityException;
import com.team5.socialnetwork.models.User;
import com.team5.socialnetwork.models.UserInfo;
import com.team5.socialnetwork.models.UserWork;
import com.team5.socialnetwork.repositories.UsersInfoRepository;
import com.team5.socialnetwork.repositories.UsersRepository;
import com.team5.socialnetwork.repositories.UsersWorkRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class UsersServiceImplTests {

    @Mock
    UsersRepository repository;

    @Mock
    UsersWorkRepository usersWorkRepository;

    @Mock
    UsersInfoRepository usersInfoRepository;

    @InjectMocks
    UsersServiceImpl mockService;

    @Test
    public void getAllUsersShould_ReturnAllUsers() {
        //Arrange
        User user = new User();
        List<User> expected = new ArrayList<>();
        expected.add(user);
        //Act
        Mockito.when(repository.findAllByEnabled(true)).thenReturn(expected);
        List<User> result = mockService.getAllUsers();
        //Assert
        Assert.assertSame(expected, result);
    }

    @Test
    public void getUserByUsernameShould_ReturnUserWhenPassedUsername() {
        //Arrange
        User expected = new User();
        //Act
        Mockito.when(repository.findByUsername(anyString())).thenReturn(expected);
        User returned = mockService.getUserByUsername(anyString());
        //Assert
        Assert.assertSame(expected, returned);
    }

    @Test
    public void getUserByEmailShould_ReturnUserWhenPassedEmail() {
        //Arrange
        User expected = new User();
        //Act
        Mockito.when(repository.findByUserInfo_Email(anyString())).thenReturn(expected);
        User returned = mockService.getUserByEmail(anyString());
        //Assert
        Assert.assertSame(expected, returned);
    }

    @Test
    public void createUserShould_CreateUserWhenPassedValidParameters() {
        //Arrange
        UserInfo user = new UserInfo();
        //Act
        mockService.createUser(user);
        //Assert
        verify(usersInfoRepository, times(1)).save(user);
    }

    @Test
    public void createUserWorkShould_CreateUserWorkWhenPassedValidParameters() {
        //Arrange
        UserWork user = new UserWork();
        //Act
        mockService.createWork(user);
        //Assert
        verify(usersWorkRepository, times(1)).save(user);
    }

    @Test
    public void updateInfoLinkShould_UpdateUser() {
        //Arrange
        UserInfo userInfo = new UserInfo();
        User user = new User();
        //Act
        Mockito.when(repository.findByUsername(anyString())).thenReturn(user);
        mockService.updateInfoLink(userInfo, anyString());
        //Assert
        verify(repository, times(1)).save(user);
    }

    @Test
    public void updateWorkLinkShould_UpdateUsersWork() {
        //Arrange
        User user = new User();
        UserInfo userInfo = new UserInfo();
        user.setUserInfo(userInfo);
        UserWork userWork = new UserWork();
        userInfo.setUserWork(userWork);
        //Act
        Mockito.when(repository.findByUsername(anyString())).thenReturn(user);
        mockService.updateWorkLink(userWork, anyString());
        //Assert
        verify(repository, times(1)).save(user);
    }

    @Test
    public void updateUserShould_UpdateUser() {
        //Arrange
        User user = new User();
        //Act
        mockService.updateUser(user);
        //Assert
        verify(repository, times(1)).save(user);
    }

    @Test
    public void findUserByResetTokenShould_ReturnUserByResetToken() {
        //Arrange
        //Act
        mockService.findUserByResetToken(anyString());
        //Assert
        verify(repository, times(1)).getByUserInfo_Token(anyString());
    }
}
