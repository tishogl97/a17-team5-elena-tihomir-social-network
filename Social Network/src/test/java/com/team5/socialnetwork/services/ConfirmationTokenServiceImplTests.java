package com.team5.socialnetwork.services;


import com.team5.socialnetwork.models.ConfirmationToken;
import com.team5.socialnetwork.repositories.ConfirmationTokenRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ConfirmationTokenServiceImplTests {

    @Mock
    ConfirmationTokenRepository repository;

    @InjectMocks
    ConfirmationTokenServiceImpl mockService;

    @Test
    public void createTokenShould_CreateToken() {
        //Arrange
        ConfirmationToken confToken = new ConfirmationToken();
        //Act
        mockService.createToken(confToken);
        //Assert
        verify(repository, times(1)).save(confToken);
    }

    @Test
    public void getByTokenShould_ReturnTokenByText() {
        //Arrange
        ConfirmationToken expected = new ConfirmationToken();
        //Act
        Mockito.when(repository.findByConfirmationToken(anyString())).thenReturn(expected);
        ConfirmationToken returned = mockService.getByToken(anyString());
        //Assert
        Assert.assertSame(expected, returned);
    }

    @Test
    public void deleteConfirmationTokenShould_DeleteConfirmationToken() {
        //Arrange
        ConfirmationToken confToken = new ConfirmationToken();
        //Act
        mockService.deleteToken(confToken.getConfirmationToken());
        //Assert
        verify(repository, times(1)).deleteByConfirmationToken(confToken.getConfirmationToken());
    }
}
