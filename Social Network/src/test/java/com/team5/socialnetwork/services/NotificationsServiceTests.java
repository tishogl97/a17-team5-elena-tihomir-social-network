package com.team5.socialnetwork.services;

import com.team5.socialnetwork.models.Notifications;
import com.team5.socialnetwork.models.Posts;
import com.team5.socialnetwork.models.UserInfo;
import com.team5.socialnetwork.repositories.NotificationsRepository;
import com.team5.socialnetwork.repositories.PostsRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static com.team5.socialnetwork.PostsTestFactory.createPost;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class NotificationsServiceTests {

    @Mock
    NotificationsRepository repository;

    @InjectMocks
    NotificationsServiceImpl mockService;

    @Test
    public void saveNotificationShould_CreateNotification() {
        //Arrange
        Notifications notification = new Notifications();
        //Act
        mockService.saveNotification(notification);
        //Assert
        verify(repository, times(1)).save(notification);
    }

    @Test
    public void deleteNotificationShould_DeleteNotification() {
        //Arrange
        Notifications notification = new Notifications();
        //Act
        mockService.deleteNotification(notification);
        //Assert
        verify(repository, times(1)).delete(notification);
    }

    @Test
    public void getAllNotificationsByUserShould_GetAllNotificationsWhenPassedValidUser() {
        //Arrange
        UserInfo user = new UserInfo();
        Notifications notification = new Notifications();
        notification.setUser(user);
        List<Notifications> expected = new ArrayList<>();
        expected.add(notification);
        //Act
        Mockito.when(repository.findAllByUser(user)).thenReturn(expected);
        List<Notifications> returned = mockService.getAllByUser(user);
        //Assert
        Assert.assertSame(expected, returned);
    }

    @Test
    public void getAllUnreadNotificationsByUserShould_GetAllUnreadNotificationsWhenPassedValidUser() {
        //Arrange
        UserInfo user = new UserInfo();
        Notifications notification = new Notifications();
        notification.setUser(user);
        notification.setRead(false);
        List<Notifications> expected = new ArrayList<>();
        expected.add(notification);
        //Act
        Mockito.when(repository.getAllUnread(user)).thenReturn(expected);
        List<Notifications> returned = mockService.getAllUnread(user);
        //Assert
        Assert.assertSame(expected, returned);
    }

    @Test
    public void findByIdShould_ReturnNotificationByIdWhenPassedValidId() {
        //Arrange
        Notifications expected = new Notifications();
        //Act
        Mockito.when(repository.findById(anyLong())).thenReturn(expected);
        Notifications returned = mockService.getById(1);
        //Assert
        Assert.assertSame(expected, returned);
    }

}
