package com.team5.socialnetwork;

import com.team5.socialnetwork.models.Posts;
import com.team5.socialnetwork.models.UserInfo;

public class PostsTestFactory {

    public static Posts createPost() {
        UserInfo user = new UserInfo(1);
        Posts post = new Posts("text", user);
        return post;
    }

    public static UserInfo createCreator() {
        UserInfo user = new UserInfo(1);
        return user;
    }
}
