// document.getElementById('edit-modal').addEventListener('click' , function () {
//     document.querySelector('.bg-modal').style.display = 'flex';
// })
// document.querySelector('.close-modal').addEventListener('click' , function () {
//     document.querySelector('.bg-modal').style.display = 'none';
// })
// $( 'document' ).ready(function() {
//     $( '.post-content .btn').on( 'click' , function(event) {
//         event.preventDefault();
//         let href= $(this).attr('href');
//         $.get(href, function(post){
//             $( '#text' ).val(post.text);
//         });
//         $( '#myModal' ).modal();
//     });
//
// });
$(function() {
    $('.eBtn').on('click', function (event) {
        event.preventDefault();
        let href = $(this).attr('href');
        $.get(href, function(post){
           $('.myModal #text').val(post.text);
        });
            $('.myModal #exampleModal').modal();
    });

});


// Show Comments Button function
$(function () {
    $(document).on('click', '.show-comments',function () {
        $(this).siblings('.comments').toggle(500)
    });
});

$(function () {
    $(document).on('click', '.editComment', function () {
        $(this).siblings('.editCommnt').toggle(500)
    });
});

$(function () {
    $(function postComment() {
        $(document).on("click", ".add-comment", (ev) => {
            ev.preventDefault();
            let form = $(ev.currentTarget).parent().parent();

            let postId = form.find('.postId').val();
            let userId = form.find('.userId').val();
            let text = form.find('.commentTxt').val();
            let creatorId = form.find('.creatorId').val();

            let data = {
                postId,
                userId,
                creatorId,
                text
            };
            $.ajax({
                type: 'POST',
                url: "http://localhost:8080/api/comments",
                data: data,
                contentType: "application/x-www-form-urlencoded",
                success: function (response) {
                    // form.parent().prev().append(response);
                    $(form).parent().prev().children().empty();

                    $("#comment-section" + postId).load(document.URL + " #comment-section" + postId);
                }
            });
        });
    })();
});

$(function () {
    $(function likePost() {
        $(document).on("click", ".likeButton", (ev) => {
            ev.preventDefault();
            let form = $(ev.currentTarget).parent();

            let postId = form.find('.postId').val();
            let userId = form.find('.userId').val();
            let status = form.find('.status').val();
            let creatorId = form.find('.creatorId').val();

            let data = {
                postId,
                userId,
                creatorId,
                status
            };
            $.ajax({
                type: 'POST',
                url: "http://localhost:8080/api/feed/thumb",
                data: data,
                // contentType: "application/x-www-form-urlencoded",
                success: function (response) {

                    $("#likes-count" + postId).load(" #likes-count" + postId);
                    $("#dislikes-count" + postId).load(" #dislikes-count" + postId);
                }
            });
        });
    })();
});

$(function () {
    $(function dislikePost() {
        $(document).on("click", ".dislikeButton", (ev) => {
            ev.preventDefault();
            let form = $(ev.currentTarget).parent();

            let postId = form.find('.postId').val();
            let userId = form.find('.userId').val();
            let status = form.find('.status').val();
            let creatorId = form.find('.creatorId').val();

            let data = {
                postId,
                userId,
                creatorId,
                status
            };
            $.ajax({
                type: 'POST',
                url: "http://localhost:8080/api/feed/thumb",
                data: data,
                // contentType: "application/x-www-form-urlencoded",
                success: function (response) {


                    $("#likes-count" + postId).load(" #likes-count" + postId);
                    $("#dislikes-count" + postId).load(" #dislikes-count" + postId);
                }
            });
        });
    })();
});

$(function () {
    $(function editComment() {
        $(document).on("click", ".edit-comment", (ev) => {
            ev.preventDefault();
            let form = $(ev.currentTarget).parent();

            let cmntId = form.find('.cmntId').val();
            // let userId = form.find('.userId').val();
            let text = form.find('.text').val();

            let data = {
                cmntId,
                // userId,
                text
            };
            $.ajax({
                type: 'POST',
                url: "http://localhost:8080/api/comments/edit",
                data: data,
                // contentType: "application/x-www-form-urlencoded",
                success: function (response) {


                    $("#comment-text" + cmntId).load(" #comment-text" + cmntId);
                    // $("#dislikes-count" + postId).load(" #dislikes-count" + postId);
                }
            });
        });
    })();
});

$(function () {
    $(function deleteComment() {
        $(document).on("click", ".deleteComment", (ev) => {
            ev.preventDefault();
            let form = $(ev.currentTarget).parent();

            let cmntId = form.find('.cmntId').val();
            let postId = form.find('.postId').val();

            let data = {
                cmntId,
                postId
            };
            $.ajax({
                type: 'DELETE',
                url: "http://localhost:8080/api/comments/delete",
                data: data,
                contentType: "application/x-www-form-urlencoded",
                success: function (response) {
                    $(form).parent().parent().empty();
                }
            });
        });
    })();
});

$(function () {
    let page = 1;

    $(document).on("click", ".show-more-posts", (ev) => {
        page++;
        // let currentScroll = $(window).scrollTop();
        let target = $(ev.currentTarget).prev();
        let data = {
            page
        };

        $.ajax({
            type: 'GET',
            url: "http://localhost:8080/news-feed",
            data: data,
            success: function (response) {
                console.log(response)
                // $(window).scrollTop(currentScroll);
                $(target).append(response).html();
            }
        })
    });
});

$(function () {
    let page = 1;

    $(document).on("click", ".shw-personal", (ev) => {
        page++;
        // let currentScroll = $(window).scrollTop();
        let target = $(ev.currentTarget).prev().prev();
        let username = $(ev.currentTarget).prev().val();
        let data = {
            username,
            page
        };

        $.ajax({
            type: 'GET',
            url: "http://localhost:8080/user/personal-feed",
            data: data,
            success: function (response) {
                console.log(response);
                // $(window).scrollTop(currentScroll);
                $(target).append(response).html();
            }
        })
    });
});

// OMDB

$(document).on('click', '.favourite-movie-button', function(ev) {
    const clickedButton = $(ev.target);
    const movieId = clickedButton.data('movieId'); // get the data in this button
    const movieTitle = clickedButton.data('movieTitle');
    const moviePoster = clickedButton.data('moviePoster');
    const data = {
        imdb: movieId,
        title: movieTitle,
        poster: moviePoster
    };

    // send Ajax POST
    fetch('/add-movie', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    }).then(res => {
        // handle response
    })
});


$(function() {
    const $input = $('#search-term');
    const $resultDiv = $('#result');

    $(document).on('click', '#search-btn', () => {
        $resultDiv.empty();
        const searchTitle = $input.val();

        fetch(`http://www.omdbapi.com/?apikey=ba1f4581&s=${searchTitle}`)
            .then((res) => res.json())
            .then((res) => {
                res.Search.forEach((movie) => {
                    $resultDiv.append(`<div><h5><a href="https://www.imdb.com/title/${movie.imdbID}">${movie.Title}</a></h5><p>${movie.Year}</p><img style="height: 360px; width: 240px;" src="${movie.Poster}"><br><br></div>`);

                    const button = $('<button class="favourite-movie-button" type="button">Add to Favorites</button>');
                    button.data('movieTitle', movie.Title); // save the title
                    button.data('movieId', movie.imdbID); // save the id
                    button.data('moviePoster', movie.Poster); // save the poster

                    $resultDiv.append(button);
                    $resultDiv.append(`<br><div class="line"></div><br><br>`);

                    $input.val('');
                });
            });
    })
})();