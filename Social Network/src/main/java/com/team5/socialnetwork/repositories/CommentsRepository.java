package com.team5.socialnetwork.repositories;

import com.team5.socialnetwork.models.Comments;
import com.team5.socialnetwork.models.Posts;
import com.team5.socialnetwork.models.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CommentsRepository extends JpaRepository<Comments, Long> {

    Comments findById(long id);

    List<Comments> findByPost(Posts post);

    void deleteAllByPost(Posts post);

    void deleteAllByCreator(UserInfo user);
}
