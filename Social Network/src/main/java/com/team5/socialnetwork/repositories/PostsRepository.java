package com.team5.socialnetwork.repositories;

import com.team5.socialnetwork.models.Posts;
import com.team5.socialnetwork.models.UserInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PostsRepository extends JpaRepository<Posts, Long> {

    Page<Posts> findAllByCreatorOrderByDateDesc(UserInfo user, Pageable pageable);

    List<Posts> findAllByCreator(UserInfo user);

    List<Posts> findAll();

    Posts findById(long id);

//  @Query(value = "SELECT p FROM Posts p WHERE p.creator IN (SELECT c.requestReceiver FROM Friends c where c.requestSender = :#{#user} and c.status like 'friends' ) or p.creator IN (SELECT f.requestSender FROM Friends f where f.requestReceiver = :#{#user} and f.status like 'friends') ORDER BY p.date desc")
//  Page<Posts> getAllByLoggedUser(@Param(value = "user") UserInfo user, Pageable pageable);

    @Query(value = "select social_network_system.users_posts.post_id, social_network_system.users_posts.text, social_network_system.users_posts.user_id, social_network_system.users_posts.privacy, social_network_system.users_posts.picture, social_network_system.users_posts.date_and_time from social_network_system.users_posts left join social_network_system.posts_comments pc on users_posts.post_id = pc.post_id left join social_network_system.posts_likes pl on users_posts.post_id = pl.post_id where users_posts.user_id in ( select social_network_system.users_friends.user_id from social_network_system.users_friends where friend_id = :user and status like 'friends') or users_posts.user_id in (select friend_id from social_network_system.users_friends where users_friends.user_id = :user and status like 'friends') group by users_posts.post_id order by avg(year(users_posts.date_and_time)) desc, avg(month(users_posts.date_and_time)) desc, avg(date(users_posts.date_and_time)) desc, avg(hour(users_posts.date_and_time)) desc, sum(comment_id) desc, sum(like_id) desc", countQuery = "SELECT count(*) FROM social_network_system.users_posts", nativeQuery = true)
    Page<Posts> getAllByLoggedUser(@Param(value = "user") UserInfo user, Pageable pageable);

    @Query(value = "select social_network_system.users_posts.post_id, social_network_system.users_posts.text, social_network_system.users_posts.user_id, social_network_system.users_posts.privacy, social_network_system.users_posts.picture, social_network_system.users_posts.date_and_time from social_network_system.users_posts left join social_network_system.posts_comments pc on users_posts.post_id = pc.post_id left join social_network_system.posts_likes pl on users_posts.post_id = pl.post_id where users_posts.privacy = false and picture IS NULL group by users_posts.post_id order by avg(year(users_posts.date_and_time)) desc, avg(month(users_posts.date_and_time)) desc, avg(date(users_posts.date_and_time)) desc, avg(hour(users_posts.date_and_time)) desc, sum(comment_id) desc, sum(like_id) desc limit 10", countQuery = "SELECT count(*) FROM social_network_system.users_posts ", nativeQuery = true)
    List<Posts> getTenPublic();

    @Query(value = "select count(post_id) from social_network_system.users_posts", countQuery = "SELECT count(*) FROM social_network_system.users_posts", nativeQuery = true)
    int getCount();

    void deleteAllByCreator(UserInfo user);

}
