package com.team5.socialnetwork.repositories;

import com.team5.socialnetwork.models.Notifications;
import com.team5.socialnetwork.models.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface NotificationsRepository extends JpaRepository<Notifications, Long> {

    List<Notifications> findAllByUser(UserInfo user);

    Notifications findById(long id);

    @Query(value = "select * from social_network_system.users_notifications where user_id = :user and is_read = false;", countQuery = "SELECT count(*) FROM social_network_system.users_notifications", nativeQuery = true)
    List<Notifications> getAllUnread(@Param(value = "user") UserInfo user);

}
