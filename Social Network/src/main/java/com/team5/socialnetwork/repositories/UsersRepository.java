package com.team5.socialnetwork.repositories;

import com.team5.socialnetwork.models.ConfirmationToken;
import com.team5.socialnetwork.models.Posts;
import com.team5.socialnetwork.models.User;
import com.team5.socialnetwork.models.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface UsersRepository extends JpaRepository<User, String> {

    List<User> findAllByEnabled(boolean enabled);

    User findByUsername(String username);

    User findByUsernameAndEnabled(String username, boolean enabled);

    User findByUserInfo_Email(String email);

    User findByUserInfo_EmailAndEnabled(String email, boolean enabled);

    @Query(value = "SELECT u.user FROM UserInfo u WHERE u.token.confirmationToken = :#{#token}")
    User getByUserInfo_Token(@Param(value = "token") String token);

}
