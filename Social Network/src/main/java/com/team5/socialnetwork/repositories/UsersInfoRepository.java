package com.team5.socialnetwork.repositories;

import com.team5.socialnetwork.models.MovieGenre;
import com.team5.socialnetwork.models.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UsersInfoRepository extends JpaRepository<UserInfo, Integer> {

    List<UserInfo> findAllByFirstNameLike(String name);

    List<UserInfo> findAllByLastNameLike(String name);

    UserInfo findById(int id);

}
