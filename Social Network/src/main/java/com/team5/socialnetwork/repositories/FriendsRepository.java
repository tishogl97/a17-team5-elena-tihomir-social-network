package com.team5.socialnetwork.repositories;

import com.team5.socialnetwork.models.Friends;
import com.team5.socialnetwork.models.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface FriendsRepository extends JpaRepository<Friends, Long> {

    List<Friends> findAllByRequestReceiverAndStatus(UserInfo receiver, String status);

    List<Friends> findAllByRequestSenderAndStatus(UserInfo receiver, String status);

    Friends findById(long id);

    Friends findByRequestReceiverAndRequestSender(UserInfo one, UserInfo two);

    Friends findByRequestReceiverAndRequestSenderAndStatus(UserInfo one, UserInfo two, String status);

    @Query(value = "select f from Friends f join User u on f.requestReceiver.id = :#{#id} where u.enabled = true")
    List<Friends> findByRequestReceiver(@Param(value = "id") int id);

    @Query(value = "select f from Friends f join User u on f.requestSender.id = :#{#id} where u.enabled = true")
    List<Friends> findByRequestSender(@Param(value = "id") int id);

    @Query(value = "select count(id) from social_network_system.users_friends", countQuery = "SELECT count(*) FROM social_network_system.users_friends", nativeQuery = true)
    int getCount();

}
