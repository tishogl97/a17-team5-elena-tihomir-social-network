package com.team5.socialnetwork.repositories;

import com.team5.socialnetwork.models.UserWork;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersWorkRepository extends JpaRepository<UserWork, Integer> {



}
