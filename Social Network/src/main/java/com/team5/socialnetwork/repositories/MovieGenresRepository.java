package com.team5.socialnetwork.repositories;

import com.team5.socialnetwork.models.MovieGenre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MovieGenresRepository extends JpaRepository<MovieGenre, Integer> {

    MovieGenre findByGenre(String genre);

}
