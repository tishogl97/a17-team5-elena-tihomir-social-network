package com.team5.socialnetwork.repositories;

import com.team5.socialnetwork.models.Likes;
import com.team5.socialnetwork.models.Posts;
import com.team5.socialnetwork.models.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LikesRepository extends JpaRepository<Likes, Long> {

    Likes findByUserAndPost(UserInfo user, Posts post);

    int countAllByPostAndStatus(Posts post, String status);

    Likes findByPost(long postId);

    void deleteAllByPost(Posts post);

    void deleteAllByUser(UserInfo user);

}
