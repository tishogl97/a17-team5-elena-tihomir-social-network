package com.team5.socialnetwork.repositories;

import com.team5.socialnetwork.models.Movie;
import com.team5.socialnetwork.models.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MoviesRepository extends JpaRepository<Movie, Long> {

    Movie findByTitle(String title);

    @Query(value = "select * from social_network_system.users_movies where user_id = :userId", nativeQuery = true)
    List<Movie> findAllByUser(@Param(value = "userId") int id);

    @Modifying
    @Query(value = "delete from social_network_system.users_movies where movie_id = :movieId and user_id = :userId", nativeQuery = true)
    void removeFromFavorites(@Param(value = "movieId") long movieId, @Param(value = "userId") int userId);

}
