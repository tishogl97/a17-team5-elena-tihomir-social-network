package com.team5.socialnetwork.exceptions;

public class AlreadyConnectedException extends RuntimeException {

    public AlreadyConnectedException(String message) {
        super(message);
    }

}
