package com.team5.socialnetwork.services;

import com.team5.socialnetwork.models.Friends;
import com.team5.socialnetwork.models.UserInfo;
import org.springframework.security.core.userdetails.User;

import java.util.List;

public interface FriendsService {

    void sendFriendRequest(Friends friend);

    List<Friends> getAllRequests(UserInfo user);

    List<Friends> getAllRequestsWithAllStatusTypes(UserInfo user);

    List<UserInfo> getAllFriends(UserInfo user);

    Friends findById(long id);

    void acceptRequest(Friends request);

    void declineRequest(Friends request);

    String checkForConnection(UserInfo logged, UserInfo profile);

    void removeFriend(UserInfo logged, UserInfo profile);

    void removeAllFriends(UserInfo user);

    int getFriendsCount();

}
