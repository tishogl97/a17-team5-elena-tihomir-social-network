package com.team5.socialnetwork.services;

import com.team5.socialnetwork.models.Comments;
import com.team5.socialnetwork.models.Posts;
import com.team5.socialnetwork.models.UserInfo;

import java.util.List;

public interface CommentsService {

    void saveComment(Comments comment);

    void editComment(Comments comment);

    Comments findById(long id);

    List<Comments> findByPost(Posts post);

    void deleteCommentsByPost(Posts post);

    void deleteCommentsByUser(UserInfo user);

    void deleteComment(Comments comment);
}
