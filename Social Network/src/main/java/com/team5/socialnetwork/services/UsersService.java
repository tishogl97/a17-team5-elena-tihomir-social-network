package com.team5.socialnetwork.services;

import com.team5.socialnetwork.models.*;

import java.util.List;
import java.util.Optional;

public interface UsersService {

    List<User> getAllUsers();

    User getUserByUsername(String username);

    User getUserByEmail(String email);

    void createUser(UserInfo userInfo);

    void createWork(UserWork userWork);

    void updateInfoLink(UserInfo userInfo, String name);

    void updateWorkLink(UserWork userWork, String name);

    void updateUser(User user);

     User findUserByResetToken(String resetToken);

}
