package com.team5.socialnetwork.services;

import com.team5.socialnetwork.models.MovieGenre;
import com.team5.socialnetwork.repositories.MovieGenresRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MovieGenresServiceImpl implements MovieGenresService {

    private MovieGenresRepository genresRepository;

    @Autowired
    public MovieGenresServiceImpl(MovieGenresRepository genresRepository) {
        this.genresRepository = genresRepository;
    }

    public List<MovieGenre> getAll() {
        return genresRepository.findAll();
    }

    @Override
    public MovieGenre getByName(String name) {
        return genresRepository.findByGenre(name);
    }
}
