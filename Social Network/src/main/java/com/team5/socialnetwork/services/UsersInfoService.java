package com.team5.socialnetwork.services;

import com.team5.socialnetwork.models.UserInfo;

import java.util.List;
import java.util.Map;

public interface UsersInfoService {

    Map<Integer, UserInfo> getAllByName(String name);

    List<UserInfo> getAllByNameAndStatusFriends(UserInfo logged, String name);

    List<UserInfo> getAllByNameAndOtherStatus(UserInfo logged, String name);

    UserInfo getByUserId(int id);
}
