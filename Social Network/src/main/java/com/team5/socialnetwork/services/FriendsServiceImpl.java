package com.team5.socialnetwork.services;

import com.team5.socialnetwork.exceptions.AlreadyConnectedException;
import com.team5.socialnetwork.models.Friends;
import com.team5.socialnetwork.models.UserInfo;
import com.team5.socialnetwork.repositories.FriendsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static com.team5.socialnetwork.constants.LoggerConstants.*;
import static com.team5.socialnetwork.utils.FriendsUtils.*;

@Service
public class FriendsServiceImpl implements FriendsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(FriendsServiceImpl.class.getName());

    private FriendsRepository friendsRepository;

    @Autowired
    public FriendsServiceImpl(FriendsRepository friendsRepository) {
        this.friendsRepository = friendsRepository;
    }

    @Override
    public void sendFriendRequest(Friends friend) {
        if (!checkForConnection(friend.getRequestSender(), friend.getRequestReceiver()).isEmpty()) {
            LOGGER.info(THROW_WHEN_USERS_ALREADY_CONNECTED);
            throw new AlreadyConnectedException(THROW_WHEN_USERS_ALREADY_CONNECTED);
        }
        friendsRepository.save(friend);
        String message = String.format(FRIEND_REQUEST_SENT_MESSAGE, friend.getId(), friend.getRequestReceiver().getFirstName(), friend.getRequestReceiver().getLastName());
        LOGGER.info(message);
    }

    @Override
    public List<Friends> getAllRequests(UserInfo user) {
        String message = String.format(FIND_ALL_REQUESTS_TO_USER_MESSAGE, user.getFirstName(), user.getLastName());
        LOGGER.info(message);
        return friendsRepository.findAllByRequestReceiverAndStatus(user, "request");
    }

    @Override
    public List<Friends> getAllRequestsWithAllStatusTypes(UserInfo user) {
        List<Friends> list = friendsRepository.findByRequestReceiver(user.getId());
        list.addAll(friendsRepository.findByRequestSender(user.getId()));
        String message = String.format(FIND_ALL_REQUESTS_AND_SENDS_OF_USER_MESSAGE, user.getFirstName(), user.getLastName());
        LOGGER.info(message);
        return list;
    }

    @Override
    public List<UserInfo> getAllFriends(UserInfo user) {
        List<Friends> requests = friendsRepository.findAllByRequestReceiverAndStatus(user, "friends");
        List<Friends> sends = friendsRepository.findAllByRequestSenderAndStatus(user, "friends");
        List<UserInfo> friends = new ArrayList<>();
        for (Friends request : requests) {
            friends.add(request.getRequestSender());
        }
        for (Friends send : sends) {
            friends.add(send.getRequestReceiver());
        }
        String message = String.format(FIND_ALL_FRIENDS_OF_USER_MESSAGE, user.getFirstName(), user.getLastName());
        LOGGER.info(message);
        return friends;
    }

    @Override
    public Friends findById(long id) {
        Friends request = friendsRepository.findById(id);
        checkIfRequestExists(request);
        String message = String.format(FIND_REQUEST_BY_ID_MESSAGE, id);
        LOGGER.info(message);
        return request;
    }

    @Override
    public void acceptRequest(Friends request) {
        friendsRepository.save(request);
        String message = String.format(ACCEPT_REQUEST_MESSAGE, request.getId());
        LOGGER.info(message);
    }

    @Override
    public void declineRequest(Friends request) {
        friendsRepository.delete(request);
        String message = String.format(DECLINE_REQUEST_MESSAGE, request.getId());
        LOGGER.info(message);
    }

    @Override
    public void removeFriend(UserInfo logged, UserInfo profile) {
        Friends friend = friendsRepository.findByRequestReceiverAndRequestSender(logged, profile);
        if (friend == null) {
            friend = friendsRepository.findByRequestReceiverAndRequestSender(profile, logged);
        }
        friendsRepository.delete(friend);
        String message = String.format(DELETE_FRIENDSHIP_MESSAGE, friend.getId());
        LOGGER.info(message);
    }

    @Override
    public void removeAllFriends(UserInfo user) {
        List<Friends> receiver = friendsRepository.findByRequestReceiver(user.getId());
        List<Friends> sender = friendsRepository.findByRequestSender(user.getId());
        receiver.addAll(sender);
        for (Friends friend : receiver) {
            friendsRepository.delete(friend);
        }
        String message = String.format(DELETE_ALL_FRIENDSHIPS_OF_USER_MESSAGE, user.getFirstName(), user.getLastName());
        LOGGER.info(message);
    }

    @Override
    public int getFriendsCount() {
        return friendsRepository.getCount();
    }

    @Override
    public String checkForConnection(UserInfo logged, UserInfo profile) {
        if (checkIfAlreadyConnected(friendsRepository.findByRequestReceiverAndRequestSender(logged, profile))) {
            return friendsRepository.findByRequestReceiverAndRequestSender(logged, profile).getStatus();
        } else if (checkIfAlreadyConnected(friendsRepository.findByRequestReceiverAndRequestSender(profile, logged))) {
            return friendsRepository.findByRequestReceiverAndRequestSender(profile, logged).getStatus();
        } else {
            return "";
        }
    }
}
