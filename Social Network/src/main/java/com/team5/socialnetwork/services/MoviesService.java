package com.team5.socialnetwork.services;

import com.team5.socialnetwork.models.Movie;
import org.springframework.stereotype.Service;

import java.util.List;

public interface MoviesService {

    void createMovie(Movie movie);

    Movie getMovieByTitle(String title);

    List<Movie> getAllMoviesByUser(int id);

    void removeFromFavorites(long movieId, int userId);

}
