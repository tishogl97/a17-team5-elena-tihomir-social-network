package com.team5.socialnetwork.services;

import com.team5.socialnetwork.models.MovieGenre;

import java.util.List;

public interface MovieGenresService {

    List<MovieGenre> getAll();

    MovieGenre getByName(String name);

}
