package com.team5.socialnetwork.services;

import com.team5.socialnetwork.models.Likes;
import com.team5.socialnetwork.models.Posts;
import com.team5.socialnetwork.models.UserInfo;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Map;

public interface LikesService {

    void thumbOnPost(Likes like);

    Map<Posts, Integer> getPostsLikes(Page<Posts> posts);

    int likesCount(Posts post);

    Map<Posts, Integer> getPostsDislikes(Page<Posts> posts);

    int dislikesCount(Posts post);

    Likes findByPostId(long postId);

    void deleteLikesByPost(Posts post);

    void deleteLikesByUser(UserInfo user);
}
