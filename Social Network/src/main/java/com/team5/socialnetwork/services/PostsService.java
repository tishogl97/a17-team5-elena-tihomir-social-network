package com.team5.socialnetwork.services;

import com.team5.socialnetwork.models.Posts;
import com.team5.socialnetwork.models.UserInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface PostsService {

    Posts findById(long id);

    void savePost(Posts post);

    void editPost(Posts post, UserInfo user, boolean isAdmin);

    void deletePost(Posts post, UserInfo user, boolean isAdmin);

    void deletePostsByUser(UserInfo user);

    Page<Posts> showPersonalFeed(UserInfo user, Pageable pageable);

    Page<Posts> getAllByLoggedUser(UserInfo user, Pageable pageable);

    List<Posts> getTenPublic();

    int getPostCount();

}
