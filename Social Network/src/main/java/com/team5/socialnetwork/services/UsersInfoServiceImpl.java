package com.team5.socialnetwork.services;

import com.team5.socialnetwork.models.UserInfo;
import com.team5.socialnetwork.repositories.FriendsRepository;
import com.team5.socialnetwork.repositories.UsersInfoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.team5.socialnetwork.constants.LoggerConstants.*;
import static com.team5.socialnetwork.utils.FriendsUtils.checkIfAlreadyConnected;
import static com.team5.socialnetwork.utils.UsersUtils.checkIfUserExists;

@Service
public class UsersInfoServiceImpl implements UsersInfoService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UsersInfoServiceImpl.class.getName());

    private UsersInfoRepository usersInfoRepository;
    private FriendsRepository friendsRepository;

    @Autowired
    public UsersInfoServiceImpl(UsersInfoRepository usersInfoRepository, FriendsRepository friendsRepository) {
        this.usersInfoRepository = usersInfoRepository;
        this.friendsRepository = friendsRepository;
    }

    @Override
    public Map<Integer, UserInfo> getAllByName(String name) {
        List<UserInfo> users = new ArrayList<>(usersInfoRepository.findAllByFirstNameLike("%" + name + "%"));
        users.addAll(usersInfoRepository.findAllByLastNameLike("%" + name + "%"));
        Map<Integer, UserInfo> map = new HashMap<>();
        for (UserInfo user : users) {
            if (user.getUser().isEnabled()) {
                map.put(user.getId(), user);
            }
        }
        String message = String.format(SEARCH_USERS_BY_NAME_MESSAGE, name);
        LOGGER.info(message);
        return map;
    }

    @Override
    public List<UserInfo> getAllByNameAndStatusFriends(UserInfo logged, String name) {
        Map<Integer, UserInfo> map = getAllByName(name);
        List<UserInfo> friends = new ArrayList<>();
        for (UserInfo profile : map.values()) {
            if(checkIfAlreadyConnected(friendsRepository.findByRequestReceiverAndRequestSenderAndStatus(logged, profile, "friends")) ||
            checkIfAlreadyConnected(friendsRepository.findByRequestReceiverAndRequestSenderAndStatus(profile, logged, "friends"))) {
                friends.add(profile);
            }
        }
        String message = String.format(SEARCH_USERS_WITH_STATUS_FRIENDS_BY_NAME_MESSAGE, name);
        LOGGER.info(message);
        return friends;
    }

    @Override
    public List<UserInfo> getAllByNameAndOtherStatus(UserInfo logged, String name) {
        Map<Integer, UserInfo> map = getAllByName(name);
        List<UserInfo> others = new ArrayList<>();
        for (UserInfo profile : map.values()) {
            if((!checkIfAlreadyConnected(friendsRepository.findByRequestReceiverAndRequestSender(logged, profile)) &&
                    !checkIfAlreadyConnected(friendsRepository.findByRequestReceiverAndRequestSender(profile, logged)))
            || (checkIfAlreadyConnected(friendsRepository.findByRequestReceiverAndRequestSenderAndStatus(logged, profile, "request")) ||
                    checkIfAlreadyConnected(friendsRepository.findByRequestReceiverAndRequestSenderAndStatus(profile, logged, "request")))) {
                others.add(profile);
            }
        }
        String message = String.format(SEARCH_USERS_WITH_OTHER_STATUS_BY_NAME_MESSAGE, name);
        LOGGER.info(message);
        return others;
    }

    @Override
    public UserInfo getByUserId(int id) {
        UserInfo user = usersInfoRepository.findById(id);
        checkIfUserExists(user);
        String message = String.format(FIND_FRIENDS_BY_ID_MESSAGE, id);
        LOGGER.info(message);
        return user;
    }

}
