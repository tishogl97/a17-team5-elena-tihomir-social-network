package com.team5.socialnetwork.services;

import com.team5.socialnetwork.models.Movie;
import com.team5.socialnetwork.repositories.MoviesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MoviesServiceImpl implements MoviesService {

    private MoviesRepository moviesRepository;

    @Autowired
    public MoviesServiceImpl(MoviesRepository moviesRepository) {
        this.moviesRepository = moviesRepository;
    }

    @Override
    public void createMovie(Movie movie) {
        moviesRepository.save(movie);
    }

    @Override
    public Movie getMovieByTitle(String title) {
        return moviesRepository.findByTitle(title);
    }

    @Override
    public List<Movie> getAllMoviesByUser(int id) {
        return moviesRepository.findAllByUser(id);
    }

    @Override
    public void removeFromFavorites(long movieId, int userId) {
        moviesRepository.removeFromFavorites(movieId, userId);
    }
}
