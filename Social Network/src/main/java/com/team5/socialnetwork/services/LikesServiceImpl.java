package com.team5.socialnetwork.services;

import com.team5.socialnetwork.models.Likes;
import com.team5.socialnetwork.models.Posts;
import com.team5.socialnetwork.models.UserInfo;
import com.team5.socialnetwork.repositories.LikesRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.team5.socialnetwork.constants.LoggerConstants.*;
import static com.team5.socialnetwork.utils.LikesUtils.checkForThumb;

@Service
public class LikesServiceImpl implements LikesService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LikesServiceImpl.class.getName());

    private LikesRepository likesRepository;

    @Autowired
    public LikesServiceImpl(LikesRepository likesRepository) {
        this.likesRepository = likesRepository;
    }

    @Override
    public void thumbOnPost(Likes like) {
        Likes check = likesRepository.findByUserAndPost(like.getUser(), like.getPost());
        if(checkForThumb(check)) {
            if(check.getStatus().equals(like.getStatus())) {
                likesRepository.delete(check);
                String message = String.format(DISLIKE_MESSAGE, check.getId());
                LOGGER.info(message);
            } else {
                check.setStatus(like.getStatus());
                likesRepository.save(check);
                String message = String.format(CREATE_LIKE_MESSAGE, check.getId());
                LOGGER.info(message);
            }
        } else {
            likesRepository.save(like);
            String message = String.format(CREATE_LIKE_MESSAGE, like.getId());
            LOGGER.info(message);
        }
    }

    @Override
    public Map<Posts, Integer> getPostsLikes(Page<Posts> posts) {
        Map<Posts, Integer> likes = new HashMap<>();
        for (Posts post : posts) {
            likes.put(post ,likesCount(post));
        }
        return likes;
    }

    @Override
    public int likesCount(Posts post) {
        return likesRepository.countAllByPostAndStatus(post, "liked");
    }

    @Override
    public Map<Posts, Integer> getPostsDislikes(Page<Posts> posts) {
        Map<Posts, Integer> dislikes = new HashMap<>();
        for (Posts post : posts) {
            dislikes.put(post , dislikesCount(post));
        }
        return dislikes;
    }

    @Override
    public int dislikesCount(Posts post) {
        return likesRepository.countAllByPostAndStatus(post, "disliked");
    }

    @Override
    public void deleteLikesByPost(Posts post) {
        likesRepository.deleteAllByPost(post);
        String message = String.format(DELETE_LIKES_BY_POST_MESSAGE, post.getId());
        LOGGER.info(message);
    }

    @Override
    public void deleteLikesByUser(UserInfo user) {
        likesRepository.deleteAllByUser(user);
        String message = String.format(DELETE_ALL_LIKES_BY_USER_MESSAGE, user.getFirstName(), user.getLastName());
        LOGGER.info(message);
    }

    @Override
    public Likes findByPostId(long postId) {
        String message = String.format(FIND_LIKE_BY_ID_MESSAGE, postId);
        LOGGER.info(message);
        return likesRepository.findByPost(postId);
    }
}
