package com.team5.socialnetwork.services;

import com.team5.socialnetwork.models.Posts;
import com.team5.socialnetwork.models.UserInfo;
import com.team5.socialnetwork.repositories.CommentsRepository;
import com.team5.socialnetwork.repositories.PostsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.team5.socialnetwork.constants.LoggerConstants.*;
import static com.team5.socialnetwork.utils.PostsUtils.checkIfPostExists;
import static com.team5.socialnetwork.utils.PostsUtils.checkPermission;

@Service
public class PostsServiceImpl implements PostsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PostsServiceImpl.class.getName());

    private PostsRepository postsRepository;
    private CommentsRepository commentsRepository;

    @Autowired
    public PostsServiceImpl(PostsRepository postsRepository, CommentsRepository commentsRepository) {
        this.postsRepository = postsRepository;
        this.commentsRepository = commentsRepository;
    }

    @Override
    public void savePost(Posts post) {
        postsRepository.save(post);
        String message = String.format(CREATE_POST_MESSAGE, post.getId());
        LOGGER.info(message);
    }

    @Override
    public void deletePost(Posts post, UserInfo user, boolean isAdmin) {
        checkPermission(post.getCreator(), user, isAdmin);
        postsRepository.delete(post);
        String message = String.format(DELETE_POST_MESSAGE, post.getId());
        LOGGER.info(message);
    }

    @Override
    public void editPost(Posts post, UserInfo user, boolean isAdmin) {
        checkPermission(post.getCreator(), user, isAdmin);
        postsRepository.save(post);
        String message = String.format(EDIT_POST_MESSAGE, post.getId());
        LOGGER.info(message);
    }

    @Override
    public void deletePostsByUser(UserInfo user) {
        List<Posts> posts = postsRepository.findAllByCreator(user);
        for (Posts post : posts) {
            commentsRepository.deleteAllByPost(post);
        }
        postsRepository.deleteAllByCreator(user);
        String message = String.format(DELETE_ALL_POSTS_BY_USER_MESSAGE, user.getFirstName(), user.getLastName());
        LOGGER.info(message);
    }


    @Override
    public Page<Posts> showPersonalFeed(UserInfo user, Pageable pageable) {
        String message = String.format(SHOW_PERSONAL_FEED_MESSAGE, user.getFirstName(), user.getLastName());
        LOGGER.info(message);
        return postsRepository.findAllByCreatorOrderByDateDesc(user, pageable);
    }


    @Override
    public Posts findById(long id) {
        Posts byId = postsRepository.findById(id);
        checkIfPostExists(byId);
        String message = String.format(FIND_POST_BY_ID_MESSAGE, id);
        LOGGER.info(message);
        return byId;
    }

    @Override
    public Page<Posts> getAllByLoggedUser(UserInfo user, Pageable pageable) {
        String message = String.format(GET_ALL_POSTS_BY_USER_MESSAGE, user.getFirstName(), user.getLastName());
        LOGGER.info(message);
        return postsRepository.getAllByLoggedUser(user, pageable);
    }

    @Override
    public List<Posts> getTenPublic() {
        return postsRepository.getTenPublic();
    }

    @Override
    public int getPostCount() {
        return postsRepository.getCount();
    }

}
