package com.team5.socialnetwork.services;

import com.team5.socialnetwork.models.Notifications;
import com.team5.socialnetwork.models.UserInfo;
import com.team5.socialnetwork.repositories.NotificationsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.team5.socialnetwork.constants.LoggerConstants.*;

@Service
public class NotificationsServiceImpl implements NotificationsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(NotificationsServiceImpl.class.getName());

    private NotificationsRepository notificationsRepository;

    @Autowired
    public NotificationsServiceImpl(NotificationsRepository notificationsRepository) {
        this.notificationsRepository = notificationsRepository;
    }

    @Override
    public void saveNotification(Notifications notification) {
        notificationsRepository.save(notification);
        String message = String.format(CREATE_NOTIFICATION_MESSAGE, notification.getId());
        LOGGER.info(message);
    }

    @Override
    public void deleteNotification(Notifications notification) {
        notificationsRepository.delete(notification);
        String message = String.format(DELETE_NOTIFICATION_MESSAGE, notification.getId());
        LOGGER.info(message);
    }

    @Override
    public List<Notifications> getAllByUser(UserInfo user) {
        String message = String.format(FIND_ALL_NOTIFICATIONS_OF_USER_MESSAGE, user.getFirstName(), user.getLastName());
        LOGGER.info(message);
        return notificationsRepository.findAllByUser(user);
    }

    @Override
    public List<Notifications> getAllUnread(UserInfo user) {
        return notificationsRepository.getAllUnread(user);
    }

    @Override
    public Notifications getById(long id) {
        String message = String.format(FIND_NOTIFICATION_BY_ID_MESSAGE, id);
        LOGGER.info(message);
        return notificationsRepository.findById(id);
    }

}
