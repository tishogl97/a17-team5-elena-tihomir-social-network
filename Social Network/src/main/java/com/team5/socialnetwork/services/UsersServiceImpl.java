package com.team5.socialnetwork.services;

import com.team5.socialnetwork.exceptions.DuplicateEntityException;
import com.team5.socialnetwork.models.*;
import com.team5.socialnetwork.repositories.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static com.team5.socialnetwork.constants.LoggerConstants.*;

@Service
public class UsersServiceImpl implements UsersService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UsersServiceImpl.class.getName());

    private UsersRepository usersRepository;
    private UsersInfoRepository usersInfoRepository;
    private UsersWorkRepository usersWorkRepository;

    @Autowired
    public UsersServiceImpl(UsersRepository usersRepository,
                            UsersInfoRepository usersInfoRepository,
                            UsersWorkRepository usersWorkRepository) {
        this.usersRepository = usersRepository;
        this.usersInfoRepository = usersInfoRepository;
        this.usersWorkRepository = usersWorkRepository;
    }

    @Override
    public List<User> getAllUsers() {
        return usersRepository.findAllByEnabled(true);
    }

    @Override
    public User getUserByUsername(String username) {
        return usersRepository.findByUsername(username);
    }

    @Override
    public User getUserByEmail(String email) {
        return usersRepository.findByUserInfo_Email(email);
    }

    @Override
    public void createUser(UserInfo userInfo) {
        if (usersRepository.findByUserInfo_EmailAndEnabled(userInfo.getEmail(), true) != null) {
            LOGGER.error(THROW_WHEN_EMAIL_ALREADY_EXISTS);
            throw new DuplicateEntityException(
                    String.format("User with email %s already exists!", userInfo.getEmail()));
        }
        usersInfoRepository.save(userInfo);
        String message = String.format(CREATE_USER_MESSAGE, userInfo.getId());
        LOGGER.info(message);
    }

    @Override
    public void createWork(UserWork userWork) {
        usersWorkRepository.save(userWork);
        String message = String.format(CREATE_WORK_MESSAGE, userWork.getId());
        LOGGER.info(message);
    }

    @Override
    public void updateInfoLink(UserInfo userInfo, String name) {
        User user = usersRepository.findByUsername(name);
        user.setUserInfo(userInfo);
        usersRepository.save(user);
        String message = String.format(UPDATE_INFO_LINK_MESSAGE, user.getUserInfo().getId());
        LOGGER.info(message);
    }

    @Override
    public void updateWorkLink(UserWork userWork, String name) {
        User user = usersRepository.findByUsername(name);
        user.getUserInfo().setUserWork(userWork);
        usersRepository.save(user);
        String message = String.format(UPDATE_WORK_LINK_MESSAGE, user.getUserInfo().getId());
        LOGGER.info(message);
    }

    @Override
    public void updateUser(User user) {
        usersRepository.save(user);
    }

    @Override
    public User findUserByResetToken(String resetToken) {
        return usersRepository.getByUserInfo_Token(resetToken);
    }
}
