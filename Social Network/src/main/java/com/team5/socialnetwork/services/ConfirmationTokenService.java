package com.team5.socialnetwork.services;

import com.team5.socialnetwork.models.ConfirmationToken;

public interface ConfirmationTokenService {

    void createToken(ConfirmationToken token);

    ConfirmationToken getByToken(String token);

    void deleteToken(String token);

}
