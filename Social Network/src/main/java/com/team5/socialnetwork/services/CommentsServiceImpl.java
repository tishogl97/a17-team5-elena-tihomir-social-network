package com.team5.socialnetwork.services;

import com.team5.socialnetwork.models.Comments;
import com.team5.socialnetwork.models.Posts;
import com.team5.socialnetwork.models.UserInfo;
import com.team5.socialnetwork.repositories.CommentsRepository;
import com.team5.socialnetwork.repositories.PostsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.team5.socialnetwork.constants.LoggerConstants.*;
import static com.team5.socialnetwork.utils.CommentsUtils.checkIfCommentExists;
import static com.team5.socialnetwork.utils.PostsUtils.checkIfPostExists;

@Service
public class CommentsServiceImpl implements CommentsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommentsServiceImpl.class.getName());

    private CommentsRepository commentsRepository;

    @Autowired
    public CommentsServiceImpl(CommentsRepository commentsRepository) {
        this.commentsRepository = commentsRepository;
    }

    @Override
    public void saveComment(Comments comment) {
        commentsRepository.save(comment);
        String message = String.format(CREATE_COMMENT_MESSAGE, comment.getId());
        LOGGER.info(message);
    }

    @Override
    public void editComment(Comments comment) {
        commentsRepository.save(comment);
        String message = String.format(EDIT_COMMENT_MESSAGE, comment.getId());
        LOGGER.info(message);
    }

    @Override
    public Comments findById(long id) {
        Comments byId = commentsRepository.findById(id);
        checkIfCommentExists(commentsRepository.findById(id));
        String message = String.format(FIND_COMMENT_BY_ID_MESSAGE, id);
        LOGGER.info(message);
        return byId;
    }

    @Override
    public List<Comments> findByPost(Posts post) {
        String message = String.format(FIND_COMMENT_BY_POST_MESSAGE, post.getId());
        LOGGER.info(message);
        return commentsRepository.findByPost(post);
    }

    @Override
    public void deleteCommentsByPost(Posts post) {
        commentsRepository.deleteAllByPost(post);
        String message = String.format(DELETE_COMMENTS_BY_POST_MESSAGE, post.getId());
        LOGGER.info(message);
    }

    @Override
    public void deleteCommentsByUser(UserInfo user) {
        commentsRepository.deleteAllByCreator(user);
        String message = String.format(DELETE_ALL_COMMENTS_BY_USER_MESSAGE, user.getFirstName(), user.getLastName());
        LOGGER.info(message);
    }

    @Override
    public void deleteComment(Comments comment) {
        commentsRepository.delete(comment);
        String message = String.format(DELETE_COMMENT_MESSAGE, comment.getId());
        LOGGER.info(message);
    }
}
