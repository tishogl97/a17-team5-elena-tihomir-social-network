package com.team5.socialnetwork.services;

import com.team5.socialnetwork.models.Notifications;
import com.team5.socialnetwork.models.Posts;
import com.team5.socialnetwork.models.UserInfo;

import java.util.List;

public interface NotificationsService {

    Notifications getById(long id);

    void saveNotification(Notifications notification);

    void deleteNotification(Notifications notification);

    List<Notifications> getAllByUser(UserInfo user);

    List<Notifications> getAllUnread(UserInfo user);

}
