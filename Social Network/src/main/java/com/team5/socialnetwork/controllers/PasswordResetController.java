package com.team5.socialnetwork.controllers;

import com.team5.socialnetwork.models.ConfirmationToken;
import com.team5.socialnetwork.models.TokenPassword;
import com.team5.socialnetwork.models.User;
import com.team5.socialnetwork.services.ConfirmationTokenService;
import com.team5.socialnetwork.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@RequestMapping("/reset-password")
public class PasswordResetController {

    private UsersService userService;
    private ConfirmationTokenService confirmationTokenService;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public PasswordResetController(UsersService userService, ConfirmationTokenService confirmationTokenService, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.confirmationTokenService = confirmationTokenService;
        this.passwordEncoder = passwordEncoder;
    }

    @ModelAttribute("passwordResetForm")
    public TokenPassword passwordReset() {
        return new TokenPassword();
    }

    @GetMapping
    public String displayResetPasswordPage(@RequestParam(required = false) String token,
                                           Model model) {

        ConfirmationToken resetToken = confirmationTokenService.getByToken(token);
        if (resetToken == null){
            model.addAttribute("error", "Could not find password reset token.");
        } else {
            model.addAttribute("token", resetToken.getConfirmationToken());
        }

        return "reset-password";
    }

    @PostMapping
    @Transactional
    public String handlePasswordReset(@ModelAttribute("passwordResetForm") @Valid TokenPassword form,
                                      BindingResult result,
                                      RedirectAttributes redirectAttributes) {

        if (result.hasErrors()){
            redirectAttributes.addFlashAttribute(BindingResult.class.getName() + ".passwordResetForm", result);
            redirectAttributes.addFlashAttribute("passwordResetForm", form);
            return "redirect:/reset-password?token=" + form.getToken();
        }

        ConfirmationToken token = confirmationTokenService.getByToken(form.getToken());
        User user = token.getUser().getUser();

        if (!form.getPassword().equals(form.getConfirmPassword())) {
            redirectAttributes.addFlashAttribute("error", "Password does not match!");
            redirectAttributes.addFlashAttribute("passwordResetForm", form);
            return "redirect:/reset-password?token=" + form.getToken();
        }

        String updatedPassword = passwordEncoder.encode(form.getPassword());
        user.setPassword(updatedPassword);
        userService.updateUser(user);
        confirmationTokenService.deleteToken(token.getConfirmationToken());
        return "reset-password-success";
    }

}

