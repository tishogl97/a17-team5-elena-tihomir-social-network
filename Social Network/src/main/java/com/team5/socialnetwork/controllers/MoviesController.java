package com.team5.socialnetwork.controllers;

import com.team5.socialnetwork.models.Movie;
import com.team5.socialnetwork.models.User;
import com.team5.socialnetwork.models.UserInfo;
import com.team5.socialnetwork.services.MoviesService;
import com.team5.socialnetwork.services.UsersInfoService;
import com.team5.socialnetwork.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.HttpServletRequest;

@Controller
public class MoviesController {

    private MoviesService moviesService;
    private UsersService usersService;

    @Autowired
    public MoviesController(MoviesService moviesService, UsersService usersService) {
        this.moviesService = moviesService;
        this.usersService = usersService;
    }

    @PostMapping("/add-movie")
    public String editMovies(@RequestBody Movie movie, HttpServletRequest request) {
        User user = usersService.getUserByUsername(request.getUserPrincipal().getName());
        UserInfo userInfo = user.getUserInfo();
        if (moviesService.getMovieByTitle(movie.getTitle()) == null) {
            Movie movieToCreate = new Movie();
            movieToCreate.setTitle(movie.getTitle());
            movieToCreate.setPoster(movie.getPoster());
            movieToCreate.setImdb(movie.getImdb());
            moviesService.createMovie(movieToCreate);
            userInfo.addMovies(movieToCreate);
        } else {
            Movie movieToAdd = moviesService.getMovieByTitle(movie.getTitle());
            userInfo.addMovies(movieToAdd);
        }
        usersService.updateUser(userInfo.getUser());
        return "redirect:/user/" + request.getUserPrincipal().getName() + "/about";
    }
}
