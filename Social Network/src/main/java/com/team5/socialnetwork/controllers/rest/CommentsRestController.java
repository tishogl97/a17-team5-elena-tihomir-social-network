package com.team5.socialnetwork.controllers.rest;

import com.team5.socialnetwork.models.*;
import com.team5.socialnetwork.services.CommentsService;
import com.team5.socialnetwork.services.NotificationsService;
import com.team5.socialnetwork.services.PostsService;
import com.team5.socialnetwork.services.UsersInfoService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.List;

@RestController
@RequestMapping("/api/comments")
public class CommentsRestController {

    private CommentsService commentsService;
    private PostsService postsService;
    private UsersInfoService usersInfoService;
    private NotificationsService notificationsService;

    public CommentsRestController(CommentsService commentsService, PostsService postsService, UsersInfoService usersInfoService, NotificationsService notificationsService) {
        this.commentsService = commentsService;
        this.postsService = postsService;
        this.usersInfoService = usersInfoService;
        this.notificationsService = notificationsService;
    }

    @PostMapping("")
    public Posts createComment(@RequestParam(value = "text") String text, @RequestParam(value = "postId") long postId,
                               @RequestParam(value = "userId") int userId, @RequestParam(value = "creatorId") int creatorId) {
        try {
            Comments toCreate = new Comments();
            Posts post = postsService.findById(postId);
            UserInfo user = usersInfoService.getByUserId(userId);
            UserInfo creator = usersInfoService.getByUserId(creatorId);
            toCreate.setText(text);
            toCreate.setPost(post);
            toCreate.setCreator(user);
            commentsService.saveComment(toCreate);
            post.addComment(toCreate);
            postsService.savePost(post);
            if (creatorId != userId) {
                Notifications notification = new Notifications();
                notification.setUser(creator);
                notification.setPost(post);
                notification.setText("" + user.getFirstName() + " " + user.getLastName() + " commented on your post.");
                notificationsService.saveNotification(notification);
            }
            return post;
        } catch (EntityNotFoundException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

    @PostMapping("/edit")
    public Comments editComment(@RequestParam(value = "cmntId") long id,
                                @RequestParam(value = "text") String text) {
        try {
            Comments toEdit = commentsService.findById(id);
            toEdit.setText(text);
            commentsService.editComment(toEdit);
            return toEdit;
        } catch (EntityNotFoundException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/post")
    public List<Comments> getAllPostComments(@RequestParam long id) {
        try {
            Posts post = postsService.findById(id);
            return commentsService.findByPost(post);
        }catch (EntityNotFoundException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

    @GetMapping("/comment")
    public Comments getCommentById(@RequestParam(value = "id") int id) {
        try {
            Comments comment = commentsService.findById(id);
            return comment;
        } catch (EntityNotFoundException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }


    @DeleteMapping("/delete")
    public void deleteComment(@RequestParam(value = "cmntId") int id) {
        try {
            Comments toDelete = commentsService.findById(id);
            commentsService.deleteComment(toDelete);
        } catch (EntityNotFoundException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }


    @DeleteMapping("/{postId}/delete")
    @Transactional
    public String deletePostComments(@PathVariable long postId) {
        try {
            Posts post = postsService.findById(postId);
            commentsService.deleteCommentsByPost(post);
            return String.format("Successfully deleted comemnts on post %d", postId);
        } catch (EntityNotFoundException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }
}
