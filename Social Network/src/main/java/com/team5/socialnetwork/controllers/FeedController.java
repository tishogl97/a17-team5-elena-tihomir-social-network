package com.team5.socialnetwork.controllers;

import com.team5.socialnetwork.exceptions.NoPermissionException;
import com.team5.socialnetwork.models.*;
import com.team5.socialnetwork.services.*;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.management.Notification;
import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import javax.validation.Valid;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import static com.team5.socialnetwork.utils.PostsUtils.checkPermission;

@Controller
@RequestMapping("/feed")
public class FeedController {

    private SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    private PostsService postsService;
    private UsersService usersService;
    private CommentsService commentsService;
    private FriendsService friendsService;
    private LikesService likesService;
    private NotificationsService notificationsService;

    public FeedController(PostsService postsService,
                          UsersService usersService,
                          CommentsService commentsService,
                          FriendsService friendsService,
                          LikesService likesService,
                          NotificationsService notificationsService) {
        this.postsService = postsService;
        this.usersService = usersService;
        this.commentsService = commentsService;
        this.friendsService = friendsService;
        this.likesService = likesService;
        this.notificationsService = notificationsService;
    }


    @PostMapping("/post")
    public String createPost(@Valid @ModelAttribute("addPost") PostsDTO dto,
                             HttpServletRequest request,
                             @RequestParam("imagefile") MultipartFile multipartFile,
                             BindingResult errors,
                             Model model) {
        if (errors.hasErrors()) {
            return "personal-feed";
        }
        try {
            Posts toCreate = new Posts();
            if (!multipartFile.isEmpty()) {
                toCreate.setPicture(Base64.getEncoder().encodeToString(multipartFile.getBytes()));
            }
            UserInfo creator = usersService.getUserByUsername(request.getUserPrincipal().getName()).getUserInfo();
            toCreate.setText(dto.getText());
            toCreate.setCreator(creator);
            toCreate.setPrivacy(dto.getPrivacy());
            toCreate.setComments(new ArrayList<>());
            postsService.savePost(toCreate);
            return "redirect:/user/" + request.getUserPrincipal().getName();
        } catch (IOException ex) {
            model.addAttribute("error", ex.getMessage());
            return "error";
        }
    }

    @GetMapping("/post/details")
    public String getPostDetails(@RequestParam long id, Model model, HttpServletRequest request) {
        try {
            User user = usersService.getUserByUsername(request.getUserPrincipal().getName());
            UserInfo userInfo = usersService.getUserByUsername(request.getUserPrincipal().getName()).getUserInfo();
            Posts postDetails = postsService.findById(id);
            model.addAttribute("postDetails", postDetails);
            model.addAttribute("dateFormat", df);
            model.addAttribute("userInfo", userInfo);
            model.addAttribute("user", user);
            model.addAttribute("addThumb", new LikesDTO());
            model.addAttribute("addComment", new CommentsDTO());
            model.addAttribute("addPost", new PostsDTO());
            model.addAttribute("likes", likesService.likesCount(postDetails));
            model.addAttribute("dislikes", likesService.dislikesCount(postDetails));
            model.addAttribute("status", friendsService.checkForConnection(postsService.findById(id).getCreator(), user.getUserInfo()));
            model.addAttribute("unread", notificationsService.getAllUnread(user.getUserInfo()));
            return "post-details";
        } catch (EntityNotFoundException ex) {
            model.addAttribute("error", ex.getMessage());
            return "error";
        }
    }

    @PostMapping("/edit/post")
    public String editPost(@RequestParam long id,
                           @RequestBody @ModelAttribute("addPost") PostsDTO dto,
                           HttpServletRequest request, BindingResult error,
                           Model model) {
        if (error.hasErrors()) {
            return "error";
        }
        try {
            UserInfo user = usersService.getUserByUsername(request.getUserPrincipal().getName()).getUserInfo();
            boolean isAdmin = request.isUserInRole("ROLE_ADMIN");
            Posts post = postsService.findById(id);
            post.setText(dto.getText());
            postsService.editPost(post, user, isAdmin);
            return "redirect:/feed/post/details?id=" + id;
        } catch (EntityNotFoundException | NoPermissionException ex) {
            model.addAttribute("error", ex.getMessage());
            return "error";
        }
    }

    @PostMapping("/delete/post")
    @Transactional
    public String deletePost(@RequestParam long id, HttpServletRequest request, Model model) {
        try {
            Posts post = postsService.findById(id);
            UserInfo user = usersService.getUserByUsername(request.getUserPrincipal().getName()).getUserInfo();
            boolean isAdmin = request.isUserInRole("ROLE_ADMIN");
            likesService.deleteLikesByPost(post);
            commentsService.deleteCommentsByPost(post);
            postsService.deletePost(post, user, isAdmin);
            return "redirect:/user/" + request.getUserPrincipal().getName();
        } catch (EntityNotFoundException | NoPermissionException ex) {
            model.addAttribute("error", ex.getMessage());
            return "error";
        }
    }


    @GetMapping("/image")
    public void renderFeedImage(@RequestParam long id, HttpServletResponse response) throws IOException {
        Posts post = postsService.findById(id);
        if (post.getPicture() != null) {
            response.setContentType("image/jpeg");
            InputStream is = new ByteArrayInputStream(Base64.getDecoder().decode(post.getPicture()));
            IOUtils.copy(is, response.getOutputStream());
        }
    }

}
