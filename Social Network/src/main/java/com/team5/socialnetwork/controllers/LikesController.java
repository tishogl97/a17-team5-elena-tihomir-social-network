package com.team5.socialnetwork.controllers;

import com.team5.socialnetwork.models.*;
import com.team5.socialnetwork.services.LikesService;
import com.team5.socialnetwork.services.NotificationsService;
import com.team5.socialnetwork.services.PostsService;
import com.team5.socialnetwork.services.UsersService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@RequestMapping("/thumb")
public class LikesController {

    private LikesService likesService;
    private PostsService postsService;
    private UsersService usersService;
    private NotificationsService notificationsService;

    public LikesController(LikesService likesService,
                           PostsService postsService,
                           UsersService usersService,
                           NotificationsService notificationsService) {
        this.likesService = likesService;
        this.postsService = postsService;
        this.usersService = usersService;
        this.notificationsService = notificationsService;
    }

    @PostMapping("/{username}")
    public String likePost(@Valid @ModelAttribute("addThumb") LikesDTO dto,
                           HttpServletRequest request, @PathVariable String username) {
        Likes toCreate = new Likes();
        UserInfo user = usersService.getUserByUsername(request.getUserPrincipal().getName()).getUserInfo();
        Posts post = postsService.findById(dto.getPostId());
        toCreate.setPost(post);
        toCreate.setUser(user);
        toCreate.setStatus(dto.getStatus());
        likesService.thumbOnPost(toCreate);
        if (post.getCreator() != user) {
            Notifications notification = new Notifications();
            notification.setUser(post.getCreator());
            notification.setPost(post);
            notification.setText("" + user.getFirstName() + " " + user.getLastName() + " reacted to your post.");
            notificationsService.saveNotification(notification);
        }
        return "redirect:/feed/post/details?id=" + dto.getPostId();
    }

}
