package com.team5.socialnetwork.controllers;

import com.team5.socialnetwork.models.Friends;
import com.team5.socialnetwork.models.UserInfo;
import com.team5.socialnetwork.services.FriendsService;
import com.team5.socialnetwork.services.UsersService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/friends")
public class FriendsController {

    private FriendsService friendsService;
    private UsersService usersService;

    public FriendsController(FriendsService friendsService, UsersService usersService) {
        this.friendsService = friendsService;
        this.usersService = usersService;
    }

    @PostMapping("/request/{receiver}")
    public String sendFriendRequest(HttpServletRequest request, @PathVariable String receiver) {
        Friends newConnection = new Friends();
        UserInfo requestSender = usersService.getUserByUsername(request.getUserPrincipal().getName()).getUserInfo();
        UserInfo requestReceiver = usersService.getUserByUsername(receiver).getUserInfo();
        newConnection.setRequestReceiver(requestReceiver);
        newConnection.setRequestSender(requestSender);
        newConnection.setStatus("request");
        friendsService.sendFriendRequest(newConnection);
        return "redirect:/user/" + receiver;
    }

    @PostMapping("/accept")
    public String acceptFriendRequest(@RequestParam long id, HttpServletRequest request) {
        try {
            String username = request.getUserPrincipal().getName();
            Friends toAccept = friendsService.findById(id);
            toAccept.setStatus("friends");
            friendsService.acceptRequest(toAccept);
            return "redirect:/user/" + username;
        }catch (EntityNotFoundException ex) {
            return "error";
        }
    }

    @PostMapping("/decline")
    public String declineFriendRequest(@RequestParam long id, HttpServletRequest request) {
        try {
            String username = request.getUserPrincipal().getName();
            Friends toDecline = friendsService.findById(id);
            friendsService.declineRequest(toDecline);
            return "redirect:/user/" + username;
        } catch (EntityNotFoundException ex) {
            return "error";
        }
    }
    @PostMapping("/remove/{profile}")
    public String removeFriend(@PathVariable String profile, HttpServletRequest request) {
        UserInfo logged = usersService.getUserByUsername(request.getUserPrincipal().getName()).getUserInfo();
        UserInfo profileUser = usersService.getUserByUsername(profile).getUserInfo();
        friendsService.removeFriend(logged, profileUser);
        return "redirect:/user/" + profile;
    }
}
