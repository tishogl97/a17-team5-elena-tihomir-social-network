package com.team5.socialnetwork.controllers.rest;

import com.team5.socialnetwork.models.User;
import com.team5.socialnetwork.models.UserInfo;
import com.team5.socialnetwork.services.UsersInfoService;
import com.team5.socialnetwork.services.UsersService;
import com.team5.socialnetwork.utils.UsersUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/user")
public class UsersRestController {

    private UsersService usersService;
    private UsersInfoService usersInfoService;

    @Autowired
    public UsersRestController(UsersService usersService, UsersInfoService usersInfoService) {
        this.usersService = usersService;
        this.usersInfoService = usersInfoService;
    }

    @GetMapping("/all")
    public List<User> getAllUsers() {
        return usersService.getAllUsers();
    }

    @GetMapping("/{username}")
    public User getUserByUsername(@PathVariable String username) {
        return usersService.getUserByUsername(username);
    }

    @PutMapping("/{username}/update")
    public String updateUser(@PathVariable String username, @RequestBody UserInfo userInfo) {
        User userToUpdate = usersService.getUserByUsername(username);
        UserInfo updated = UsersUtils.mapUserInfo(userToUpdate, userInfo);
        userToUpdate.setUserInfo(updated);
        usersService.updateUser(userToUpdate);
        return "Successfully updated user!";
    }

    @GetMapping
    public UserInfo getByUserId(@RequestParam int id) {
        try {
            return usersInfoService.getByUserId(id);
        } catch (EntityNotFoundException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

    @GetMapping("/search")
    public Map<Integer, UserInfo> searchUsersByName(@RequestParam String usernameToFind) {
        return usersInfoService.getAllByName(usernameToFind);
    }

    @GetMapping("/{username}/search/friends")
    public List<UserInfo> searchUsersByNameAndStatusFriends(@PathVariable String username,
                                                            @RequestParam String usernameToFind) {
        UserInfo user = usersService.getUserByUsername(username).getUserInfo();
        return usersInfoService.getAllByNameAndStatusFriends(user, usernameToFind);
    }

    @GetMapping("/{username}/search/others")
    public List<UserInfo> searchUsersByNameAndOtherStatus(@PathVariable String username,
                                                          @RequestParam String usernameToFind) {
        UserInfo user = usersService.getUserByUsername(username).getUserInfo();
        return usersInfoService.getAllByNameAndOtherStatus(user, usernameToFind);
    }
}
