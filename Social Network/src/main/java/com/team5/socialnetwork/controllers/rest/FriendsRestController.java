package com.team5.socialnetwork.controllers.rest;

import com.team5.socialnetwork.exceptions.AlreadyConnectedException;
import com.team5.socialnetwork.models.Friends;
import com.team5.socialnetwork.models.UserInfo;
import com.team5.socialnetwork.services.FriendsService;
import com.team5.socialnetwork.services.UsersInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@RestController
@RequestMapping("/api/connection")
public class FriendsRestController {

    private FriendsService friendsService;
    private UsersInfoService usersInfoService;

    @Autowired
    public FriendsRestController(FriendsService friendsService, UsersInfoService usersInfoService) {
        this.friendsService = friendsService;
        this.usersInfoService = usersInfoService;
    }

    @PostMapping("/{sent}/send/{receive}")
    public Friends sendFriendRequest(@PathVariable int sent, @PathVariable int receive) {
        try {
            UserInfo sender = usersInfoService.getByUserId(sent);
            UserInfo receiver = usersInfoService.getByUserId(receive);
            Friends newConnection = new Friends();
            newConnection.setRequestSender(sender);
            newConnection.setRequestReceiver(receiver);
            newConnection.setStatus("request");
            friendsService.sendFriendRequest(newConnection);
            return newConnection;
        } catch (EntityNotFoundException | AlreadyConnectedException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }
    @PostMapping("/accept")
    public Friends acceptRequest(@RequestParam int id) {
        try {
            Friends toAccept = friendsService.findById(id);
            toAccept.setStatus("friends");
            friendsService.acceptRequest(toAccept);
            return toAccept;
        }catch (EntityNotFoundException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

    @DeleteMapping("/decline")
    public String declineRequest(@RequestParam int id) {
        try {
            Friends toDecline = friendsService.findById(id);
            friendsService.declineRequest(toDecline);
            return String.format("Request %d Declined!", id);
        } catch (EntityNotFoundException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

    @GetMapping("/requests")
    public List<Friends> getAllRequests(@RequestParam int userId) {
        try {
            UserInfo user = usersInfoService.getByUserId(userId);
            return friendsService.getAllRequests(user);
        }catch (EntityNotFoundException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

    @GetMapping("/friends")
    public List<UserInfo> getAllFriends(@RequestParam int userId) {
        try {
            UserInfo user = usersInfoService.getByUserId(userId);
            return friendsService.getAllFriends(user);
        }catch (EntityNotFoundException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

    @DeleteMapping("/{user}/remove/{toRemove}")
    public String removeFriend(@PathVariable int user, @PathVariable int toRemove) {
        try {
            UserInfo userOne = usersInfoService.getByUserId(user);
            UserInfo userTwo = usersInfoService.getByUserId(toRemove);
            friendsService.removeFriend(userOne, userTwo);
            return String.format("Successfully removed connection!");
        } catch (EntityNotFoundException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

    @DeleteMapping("/{userId}/removeAll")
    public String removeAllUserFriends(@PathVariable int userId) {
        try {
            UserInfo user = usersInfoService.getByUserId(userId);
            friendsService.removeAllFriends(user);
            return String.format("All friends of user %d were removed!", userId);
        } catch (EntityNotFoundException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }
}
