package com.team5.socialnetwork.controllers.rest;

import com.team5.socialnetwork.models.*;
import com.team5.socialnetwork.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.util.List;

@RestController
@RequestMapping("/api/feed")
public class PostsRestController {

    private PostsService postsService;
    private UsersInfoService usersInfoService;
    private CommentsService commentsService;
    private LikesService likesService;
    private NotificationsService notificationsService;

    @Autowired
    public PostsRestController(PostsService postsService, UsersInfoService usersInfoService,
                               CommentsService commentsService, LikesService likesService,
                               NotificationsService notificationsService) {
        this.postsService = postsService;
        this.usersInfoService = usersInfoService;
        this.commentsService = commentsService;
        this.likesService = likesService;
        this.notificationsService = notificationsService;
    }

    @PostMapping("/post")
    public String createPost(@RequestBody PostsDTO dto, @RequestParam Integer userId) {
        Posts toCreate = new Posts();
        UserInfo user = usersInfoService.getByUserId(userId);
        toCreate.setText(dto.getText());
        toCreate.setPrivacy(dto.getPrivacy());
        toCreate.setCreator(user);
        postsService.savePost(toCreate);
        return "Successfully created Post!";
    }

    @PostMapping("/edit/{id}")
    public String editPost(@RequestBody PostsDTO dto, @PathVariable long id) {
        try {
            Posts toEdit = postsService.findById(id);
            toEdit.setText(dto.getText());
            postsService.savePost(toEdit);
        } catch (EntityNotFoundException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
        return "Successfully Edited Post!";
    }

    @GetMapping("/post")
    public Posts getPostById(@RequestParam long id) {
        try {
            Posts post = postsService.findById(id);
            return post;
        } catch (EntityNotFoundException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

    @DeleteMapping("/post/delete")
    @Transactional
    public String deletePost(@RequestParam long id) {
        try {
            Posts post = postsService.findById(id);
            commentsService.deleteCommentsByPost(post);
            postsService.deletePost(post, post.getCreator(), false);
            return String.format("Successfully deleted post %d!", id);
        } catch (EntityNotFoundException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/thumb")
    public Likes thumbOnPost(@RequestParam(value = "postId") int postId,
                             @RequestParam(value = "userId") int userId,
                             @RequestParam(value = "status") String status,
                             @RequestParam(value ="creatorId") int creatorId) {
        try {
            UserInfo user = usersInfoService.getByUserId(userId);
            UserInfo creator = usersInfoService.getByUserId(creatorId);
            Posts post = postsService.findById(postId);
            Likes thumb = new Likes();
            thumb.setPost(post);
            thumb.setUser(user);
            thumb.setStatus(status);
            likesService.thumbOnPost(thumb);
            if (creatorId != userId) {
                Notifications notification = new Notifications();
                notification.setUser(creator);
                notification.setPost(post);
                notification.setText("" + user.getFirstName() + " " + user.getLastName() + " reacted to your post.");
                notificationsService.saveNotification(notification);
            }
            return thumb;
        } catch (EntityNotFoundException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }
    @GetMapping("/user")
    public Page<Posts> showPersonalFeed(@RequestParam int userId, @RequestParam int page, HttpServletRequest request) {
        try {
            int size = 10;

            if (request.getParameter("page") != null && !request.getParameter("page").isEmpty()) {
                page = Integer.parseInt(request.getParameter("page")) - 1;
            }

            if (request.getParameter("size") != null && !request.getParameter("size").isEmpty()) {
                size = Integer.parseInt(request.getParameter("size"));
            }

            UserInfo user = usersInfoService.getByUserId(userId);
            Page<Posts> myFeed = postsService.showPersonalFeed(user, PageRequest.of(page, size));
            return myFeed;
        } catch (EntityNotFoundException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

    @GetMapping("/news")
    public Page<Posts> showNewsFeed(@RequestParam int userId, @RequestParam int page, HttpServletRequest request) {
        try {
            int size = 10;

            if (request.getParameter("page") != null && !request.getParameter("page").isEmpty()) {
                page = Integer.parseInt(request.getParameter("page")) - 1;
            }

            if (request.getParameter("size") != null && !request.getParameter("size").isEmpty()) {
                size = Integer.parseInt(request.getParameter("size"));
            }

            UserInfo user = usersInfoService.getByUserId(userId);
            Page<Posts> myFeed = postsService.getAllByLoggedUser(user, PageRequest.of(page, size));
            return myFeed;
        } catch (EntityNotFoundException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }
}
