package com.team5.socialnetwork.controllers;

import com.team5.socialnetwork.models.*;
import com.team5.socialnetwork.services.*;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
public class HomeController {

    private UsersService usersService;
    private UsersInfoService usersInfoService;
    private PostsService postsService;
    private LikesService likesService;
    private NotificationsService notificationsService;
    private FriendsService friendsService;
    private MoviesService moviesService;
    private SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    @Autowired
    public HomeController(UsersService usersService,
                          PostsService postsService,
                          UsersInfoService usersInfoService,
                          LikesService likesService,
                          NotificationsService notificationsService,
                          FriendsService friendsService,
                          MoviesService moviesService) {
        this.usersService = usersService;
        this.postsService = postsService;
        this.usersInfoService = usersInfoService;
        this.likesService = likesService;
        this.notificationsService = notificationsService;
        this.friendsService = friendsService;
        this.moviesService = moviesService;
    }

    @GetMapping("/")
    public String showHomePageAfterLogin(HttpServletRequest request, Model model) {
        if (request.getUserPrincipal() != null) {

            int page = 0;
            int size = 10;

            if (request.getParameter("page") != null && !request.getParameter("page").isEmpty()) {
                page = Integer.parseInt(request.getParameter("page")) - 1;
            }

            if (request.getParameter("size") != null && !request.getParameter("size").isEmpty()) {
                size = Integer.parseInt(request.getParameter("size"));
            }

            String username = request.getUserPrincipal().getName();
            User user = usersService.getUserByUsername(username);
            Page<Posts> myFeed = postsService.getAllByLoggedUser(user.getUserInfo(), PageRequest.of(page, size));

            model.addAttribute("dateFormat", df);
            model.addAttribute("user", user);
            model.addAttribute("userInfo", user.getUserInfo());
            model.addAttribute("addPost", new PostsDTO());
            model.addAttribute("addComment", new CommentsDTO());
            model.addAttribute("newsFeed", myFeed);
            model.addAttribute("likes", likesService.getPostsLikes(myFeed));
            model.addAttribute("dislikes", likesService.getPostsDislikes(myFeed));
            model.addAttribute("unread", notificationsService.getAllUnread(user.getUserInfo()));
        }
        model.addAttribute("dateFormat", df);
        model.addAttribute("usersCount", usersService.getAllUsers().size());
        model.addAttribute("postsCount", postsService.getPostCount());
        model.addAttribute("friendsCount", friendsService.getFriendsCount());
        model.addAttribute("publicPosts", postsService.getTenPublic());
        return "index";
    }

    @GetMapping("/news-feed")
    public String getMyNewsFeed(HttpServletRequest request, @RequestParam(value = "page") int page, Model model) {

        int size = 10;

        if (request.getParameter("page") != null && !request.getParameter("page").isEmpty()) {
            page = Integer.parseInt(request.getParameter("page")) - 1;
        }

        if (request.getParameter("size") != null && !request.getParameter("size").isEmpty()) {
            size = Integer.parseInt(request.getParameter("size"));
        }

        String username = request.getUserPrincipal().getName();
        User user = usersService.getUserByUsername(username);
        Page<Posts> myFeed = postsService.getAllByLoggedUser(user.getUserInfo(), PageRequest.of(page, size));
        model.addAttribute("loadPosts", myFeed);
        model.addAttribute("dateFormat", df);
        model.addAttribute("user", user);
        model.addAttribute("userInfo", user.getUserInfo());
        model.addAttribute("addComment", new CommentsDTO());
        model.addAttribute("likes", likesService.getPostsLikes(myFeed));
        model.addAttribute("dislikes", likesService.getPostsDislikes(myFeed));
        model.addAttribute("unread", notificationsService.getAllUnread(user.getUserInfo()));

        return "news-feed-iteration";
    }

    @GetMapping("/users")
    public String searchUsers(@RequestParam(value = "word") String word, Model model) {
        model.addAttribute("resultUsers", usersInfoService.getAllByName(word));
        return "anonymous-users";
    }

    @GetMapping("/{username}")
    public String showUserPublicTimeline(@PathVariable String username, HttpServletRequest request, Model model) {

        int page = 0;
        int size = 10;

        if (request.getParameter("page") != null && !request.getParameter("page").isEmpty()) {
            page = Integer.parseInt(request.getParameter("page")) - 1;
        }

        if (request.getParameter("size") != null && !request.getParameter("size").isEmpty()) {
            size = Integer.parseInt(request.getParameter("size"));
        }

        User user = usersService.getUserByUsername(username);
        Page<Posts> myPosts = postsService.showPersonalFeed(user.getUserInfo(), PageRequest.of(page, size));
        model.addAttribute("dateFormat", df);
        model.addAttribute("user", user);
        model.addAttribute("userInfo", user.getUserInfo());
        model.addAttribute("myPosts", myPosts);
        model.addAttribute("likes", likesService.getPostsLikes(myPosts));
        model.addAttribute("dislikes", likesService.getPostsDislikes(myPosts));
        return "personal-feed-public";
    }

    @GetMapping("/{username}/about")
    public String showUserPublicAbout(@PathVariable String username, Model model) {
        User user = usersService.getUserByUsername(username);
        model.addAttribute("user", user);
        model.addAttribute("userInfo", user.getUserInfo());
        model.addAttribute("work", user.getUserInfo().getUserWork());
        model.addAttribute("movies", moviesService.getAllMoviesByUser(user.getUserInfo().getId()));
        return "personal-info-public";
    }

    @GetMapping("/image")
    public void renderUserImage(@RequestParam String username, HttpServletResponse response) throws IOException {
        User user = usersService.getUserByUsername(username);
        if (user.getUserInfo().getPicture() != null) {
            response.setContentType("image/jpeg");
            InputStream is = new ByteArrayInputStream(Base64.getDecoder().decode(user.getUserInfo().getPicture()));
            IOUtils.copy(is, response.getOutputStream());
        }
    }

    @GetMapping("/image/cover")
    public void renderCoverImage(@RequestParam String username, HttpServletResponse response) throws IOException {
        User user = usersService.getUserByUsername(username);
        if (user.getUserInfo().getCoverPicture() != null) {
            response.setContentType("image/jpeg");
            InputStream is = new ByteArrayInputStream(Base64.getDecoder().decode(user.getUserInfo().getCoverPicture()));
            IOUtils.copy(is, response.getOutputStream());
        }
    }

}
