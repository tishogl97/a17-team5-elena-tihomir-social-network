package com.team5.socialnetwork.controllers;

import com.team5.socialnetwork.models.*;
import com.team5.socialnetwork.services.ConfirmationTokenService;
import com.team5.socialnetwork.services.EmailSenderService;
import com.team5.socialnetwork.services.EmailSenderServiceImpl;
import com.team5.socialnetwork.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Controller
public class RegistrationController {

    private UserDetailsManager userDetailsManager;
    private PasswordEncoder passwordEncoder;
    private UsersService usersService;
    private EmailSenderService emailSenderService;
    private ConfirmationTokenService confirmationTokenService;


    @Autowired
    public RegistrationController(UserDetailsManager userDetailsManager,
                                  PasswordEncoder passwordEncoder,
                                  UsersService usersService,
                                  EmailSenderService emailSenderService,
                                  ConfirmationTokenService confirmationTokenService) {
        this.userDetailsManager = userDetailsManager;
        this.passwordEncoder = passwordEncoder;
        this.usersService = usersService;
        this.emailSenderService = emailSenderService;
        this.confirmationTokenService = confirmationTokenService;
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("user", new User());
        model.addAttribute("userInfo", new UserInfo());
        return "index-register";
    }

    @PostMapping("/register")
    public String registerUser(@ModelAttribute @Valid User user,
                               BindingResult bindingResult1,
                               @ModelAttribute @Valid UserInfo userInfo,
                               BindingResult bindingResult2,
                               @RequestParam("imagefile") MultipartFile multipartFile,
                               Model model) throws IOException {

        if (bindingResult1.hasErrors()) {
            model.addAttribute("error", "Username/password can't be less than 3 characters!");
            return "index-register";
        }

        if (bindingResult2.hasErrors()) {
            model.addAttribute("error", "First/Last name can't be less than 2 characters!");
            return "index-register";
        }

        if (userDetailsManager.userExists(user.getUsername())) {
            model.addAttribute("error", "User with the same username already exists!");
            return "index-register";
        }

        if (!user.getPassword().equals(user.getPasswordConfirmation())) {
            model.addAttribute("error", "Password does't match!");
            return "index-register";
        }

        if (usersService.getUserByEmail(userInfo.getEmail()) != null) {
            model.addAttribute("error", "User with the same email already exists!");
            return "index-register";
        }

        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User newUser =
                new org.springframework.security.core.userdetails.User(
                        user.getUsername(),
                        passwordEncoder.encode(user.getPassword()),
                        authorities);
        userDetailsManager.createUser(newUser);
        User userToDisable = usersService.getUserByUsername(user.getUsername());
        userToDisable.setEnabled(false);
        usersService.updateUser(userToDisable);

        String day = userInfo.getDay();
        String month = userInfo.getMonth();
        String year = userInfo.getYear();
        userInfo.setBirthday(day + "-" + month + "-" + year);
        usersService.createUser(userInfo);

        UserWork userWork = new UserWork();
        usersService.createWork(userWork);
        userInfo.setUserWork(userWork);

        String picture = Base64.getEncoder().encodeToString(multipartFile.getBytes());
        if (!picture.isEmpty()) {
            userInfo.setPicture(picture);
        }

        ConfirmationToken confirmationToken = new ConfirmationToken(user.getUserInfo());
        confirmationTokenService.createToken(confirmationToken);
        userInfo.setToken(confirmationToken);
        confirmationToken.setUser(userInfo);

        usersService.updateInfoLink(userInfo, user.getUsername());

        User userToSend = usersService.getUserByUsername(user.getUsername());
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(userToSend.getUserInfo().getEmail());
        mailMessage.setSubject("Complete Registration!");
        mailMessage.setFrom("elena.veskova@mail.com");
        mailMessage.setText("To confirm your account, please click here: "
                + "http://localhost:8080/confirm-account?token=" + confirmationToken.getConfirmationToken());
        emailSenderService.sendEmail(mailMessage);

        model.addAttribute("success", "Check your e-mail to confirm registration.");
        return "index-register";
    }

    @RequestMapping(value = "/confirm-account", method = {RequestMethod.GET, RequestMethod.POST})
    public String registerConfirmation(@RequestParam("token") String confirmationToken, Model model) {
        ConfirmationToken token = confirmationTokenService.getByToken(confirmationToken);
        if (token != null) {
            User user = usersService.getUserByEmail(token.getUser().getEmail());
            user.setEnabled(true);
            usersService.updateUser(user);
            confirmationTokenService.deleteToken(confirmationToken);
            model.addAttribute("success", "Welcome to the community! Use Login tab to find new movie friends.");
        } else {
            model.addAttribute("error", "The link is invalid or broken!");
        }
        return "index-register-confirmation";
    }


}
