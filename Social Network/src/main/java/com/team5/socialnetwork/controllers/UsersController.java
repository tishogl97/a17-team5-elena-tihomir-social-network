package com.team5.socialnetwork.controllers;

import com.team5.socialnetwork.models.*;
import com.team5.socialnetwork.services.*;
import com.team5.socialnetwork.utils.UsersUtils;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import sun.security.util.Password;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.List;

@Controller
@RequestMapping("/user")
public class UsersController {

    private UsersService usersService;
    private UsersInfoService usersInfoService;
    private PostsService postsService;
    private FriendsService friendsService;
    private MovieGenresService genresService;
    private MoviesService moviesService;
    private SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    private PasswordEncoder passwordEncoder;
    private LikesService likesService;
    private CommentsService commentsService;
    private NotificationsService notificationsService;

    @Autowired
    public UsersController(UsersService usersService,
                           UsersInfoService usersInfoService,
                           PostsService postsService,
                           FriendsService friendsService,
                           MovieGenresService genresService,
                           PasswordEncoder passwordEncoder,
                           LikesService likesService,
                           CommentsService commentsService,
                           NotificationsService notificationsService,
                           MoviesService moviesService) {
        this.usersService = usersService;
        this.usersInfoService = usersInfoService;
        this.postsService = postsService;
        this.friendsService = friendsService;
        this.genresService = genresService;
        this.passwordEncoder = passwordEncoder;
        this.likesService = likesService;
        this.commentsService = commentsService;
        this.notificationsService = notificationsService;
        this.moviesService = moviesService;
    }

    @GetMapping("/{username}")
    public String showUserTimeline(@PathVariable String username, HttpServletRequest request, Model model) {

        int page = 0;
        int size = 10;
        if (request.getParameter("page") != null && !request.getParameter("page").isEmpty()) {
            page = Integer.parseInt(request.getParameter("page")) - 1;
        }

        if (request.getParameter("size") != null && !request.getParameter("size").isEmpty()) {
            size = Integer.parseInt(request.getParameter("size"));
        }

        User loggedUser = usersService.getUserByUsername(request.getUserPrincipal().getName());
        User user = usersService.getUserByUsername(username);
        Page<Posts> myPosts = postsService.showPersonalFeed(user.getUserInfo(), PageRequest.of(page, size));
        model.addAttribute("dateFormat", df);
        model.addAttribute("user", user);
        model.addAttribute("userInfo", user.getUserInfo());
        model.addAttribute("addPost", new PostsDTO());
        model.addAttribute("addComment", new CommentsDTO());
        model.addAttribute("addThumb", new LikesDTO());
        model.addAttribute("myPosts", myPosts);
        model.addAttribute("loggedUser", loggedUser);
        model.addAttribute("likes", likesService.getPostsLikes(myPosts));
        model.addAttribute("dislikes", likesService.getPostsDislikes(myPosts));
        model.addAttribute("friendRequests", friendsService.getAllRequests(loggedUser.getUserInfo()));
        model.addAttribute("status", friendsService.checkForConnection(loggedUser.getUserInfo(), user.getUserInfo()));
        model.addAttribute("unread", notificationsService.getAllUnread(user.getUserInfo()));
        return "personal-feed";
    }

    @GetMapping("/personal-feed")
    public String getMyNewsFeed(HttpServletRequest request, @RequestParam(value = "page") int page,
                                @RequestParam(value = "username") String username, Model model) {

        int size = 10;
        if (request.getParameter("page") != null && !request.getParameter("page").isEmpty()) {
            page = Integer.parseInt(request.getParameter("page")) - 1;
        }

        if (request.getParameter("size") != null && !request.getParameter("size").isEmpty()) {
            size = Integer.parseInt(request.getParameter("size"));
        }

        User loggedUser = usersService.getUserByUsername(request.getUserPrincipal().getName());
        User user = usersService.getUserByUsername(username);
        Page<Posts> myPosts = postsService.showPersonalFeed(user.getUserInfo(), PageRequest.of(page, size));
        model.addAttribute("loadPosts", myPosts);
        model.addAttribute("loggedUser", loggedUser);
        model.addAttribute("dateFormat", df);
        model.addAttribute("user", user);
        model.addAttribute("status", friendsService.checkForConnection(loggedUser.getUserInfo(), user.getUserInfo()));
        model.addAttribute("userInfo", user.getUserInfo());
        model.addAttribute("addComment", new CommentsDTO());
        model.addAttribute("likes", likesService.getPostsLikes(myPosts));
        model.addAttribute("dislikes", likesService.getPostsDislikes(myPosts));
        model.addAttribute("unread", notificationsService.getAllUnread(user.getUserInfo()));

        return "personal-feed-iteration";
    }

    @GetMapping("/{username}/about")
    public String showUserAbout(@PathVariable String username, HttpServletRequest request, Model model) {
        User loggedUser = usersService.getUserByUsername(request.getUserPrincipal().getName());
        User user = usersService.getUserByUsername(username);
        model.addAttribute("user", user);
        model.addAttribute("userInfo", user.getUserInfo());
        model.addAttribute("work", user.getUserInfo().getUserWork());
        model.addAttribute("loggedUser", loggedUser);
        model.addAttribute("status", friendsService.checkForConnection(loggedUser.getUserInfo(), user.getUserInfo()));
        model.addAttribute("unread", notificationsService.getAllUnread(user.getUserInfo()));
        model.addAttribute("movies", moviesService.getAllMoviesByUser(user.getUserInfo().getId()));
        return "personal-info";
    }

    @GetMapping("/{username}/edit/basic")
    public String showEditBasic(@PathVariable String username, @ModelAttribute("error") String error,
                                HttpServletRequest request, Model model) {
        User loggedUser = usersService.getUserByUsername(request.getUserPrincipal().getName());
        User user = usersService.getUserByUsername(username);
        model.addAttribute("error", error);
        if (loggedUser != user && !request.isUserInRole("ADMIN")) {
            return "access-denied";
        }
        model.addAttribute("user", user);
        model.addAttribute("userInfo", user.getUserInfo());
        model.addAttribute("loggedUser", loggedUser);
        model.addAttribute("unread", notificationsService.getAllUnread(user.getUserInfo()));
        return "edit-profile-basic";
    }

    @PostMapping("/{username}/edit/basic")
    public String EditBasic(@PathVariable String username, @ModelAttribute @Valid UserInfo userInfo,
                            BindingResult errors, RedirectAttributes redirectAttributes) {
        if (errors.hasErrors()) {
            redirectAttributes.addFlashAttribute("error", "First/Last name can't be less than 2 characters!");
            return "redirect:/user/" + username + "/edit/basic";
        }
        User user = usersService.getUserByUsername(username);
        UserInfo userInfoToUpdate = UsersUtils.mapUserInfo(user, userInfo);
        usersService.updateInfoLink(userInfoToUpdate, username);
        return "redirect:/user/" + username + "/about";
    }

    @GetMapping("/{username}/edit/photos")
    public String showEditPhotos(@PathVariable String username, HttpServletRequest request, Model model) {
        User loggedUser = usersService.getUserByUsername(request.getUserPrincipal().getName());
        User user = usersService.getUserByUsername(username);
        if (loggedUser != user && !request.isUserInRole("ADMIN")) {
            return "access-denied";
        }
        model.addAttribute("user", user);
        model.addAttribute("userInfo", user.getUserInfo());
        model.addAttribute("loggedUser", loggedUser);
        model.addAttribute("unread", notificationsService.getAllUnread(user.getUserInfo()));
        return "edit-profile-photos";
    }

    @PostMapping("/{username}/edit/photos/profile")
    public String EditProfilePhoto(@PathVariable String username, @ModelAttribute UserInfo userInfo,
                            @RequestParam("imagefile") MultipartFile multipartFile) throws IOException {
        User user = usersService.getUserByUsername(username);
        UserInfo userInfoToUpdate = user.getUserInfo();
        if(!multipartFile.isEmpty()) {
            String picture = Base64.getEncoder().encodeToString(multipartFile.getBytes());
            userInfoToUpdate.setPicture(picture);
        }
        userInfoToUpdate.setPicturePrivacy(userInfo.isPicturePrivacy());
        usersService.updateInfoLink(userInfoToUpdate, username);
        return "redirect:/user/" + username + "/about";
    }

    @PostMapping("/{username}/edit/photos/cover")
    public String EditCoverPhoto(@PathVariable String username,
                                 @RequestParam("imagefile") MultipartFile multipartFile) throws IOException {
        User user = usersService.getUserByUsername(username);
        UserInfo userInfoToUpdate = user.getUserInfo();
        if(!multipartFile.isEmpty()) {
            String picture = Base64.getEncoder().encodeToString(multipartFile.getBytes());
            userInfoToUpdate.setCoverPicture(picture);
            usersService.updateInfoLink(userInfoToUpdate, username);
        }
        return "redirect:/user/" + username + "/about";
    }

    @GetMapping("/{username}/edit/work")
    public String showEditWork(@PathVariable String username, HttpServletRequest request, Model model) {
        User loggedUser = usersService.getUserByUsername(request.getUserPrincipal().getName());
        User user = usersService.getUserByUsername(username);
        if (loggedUser != user && !request.isUserInRole("ADMIN")) {
            return "access-denied";
        }
        model.addAttribute("user", user);
        model.addAttribute("userInfo", user.getUserInfo());
        model.addAttribute("userWork", user.getUserInfo().getUserWork());
        model.addAttribute("loggedUser", loggedUser);
        model.addAttribute("unread", notificationsService.getAllUnread(user.getUserInfo()));
        return "edit-profile-work";
    }

    @PostMapping("/{username}/edit/work")
    public String EditWork(@PathVariable String username, @ModelAttribute("userWork") UserWork userWork) {
        User user = usersService.getUserByUsername(username);
        UserWork userWorkToUpdate = UsersUtils.mapUserWork(user, userWork);
        usersService.updateWorkLink(userWorkToUpdate, username);
        return "redirect:/user/" + username + "/about";
    }

    @GetMapping("/{username}/edit/password")
    public String showChangePasswordPage(@PathVariable String username, @ModelAttribute("error") String error,
                                         HttpServletRequest request, Model model) {
        User loggedUser = usersService.getUserByUsername(request.getUserPrincipal().getName());
        User user = usersService.getUserByUsername(username);
        model.addAttribute("error", error);
        if (loggedUser != user && !request.isUserInRole("ADMIN")) {
            return "access-denied";
        }
        model.addAttribute("user", user);
        model.addAttribute("userInfo", user.getUserInfo());
        model.addAttribute("userPassword", new UserPassword());
        model.addAttribute("loggedUser", loggedUser);
        model.addAttribute("unread", notificationsService.getAllUnread(user.getUserInfo()));
        return "edit-profile-password";
    }

    @PostMapping("/{username}/edit/password")
    public String changePassword(@PathVariable String username, @ModelAttribute @Valid UserPassword password,
                                 BindingResult errors, RedirectAttributes redirectAttributes) {
        if (errors.hasErrors()) {
            redirectAttributes.addFlashAttribute("error", "Password can't be less than 3 characters!");
            return "redirect:/user/" + username + "/edit/password";
        }
        User user = usersService.getUserByUsername(username);
        if (!passwordEncoder.matches(password.getCurrentPassword(), user.getPassword()) ||
                !password.getNewPassword().equals(password.getRepeatNewPassword())) {
            redirectAttributes.addFlashAttribute("error", "Password does't match!");
            return "redirect:/user/" + username + "/edit/password";
        }
        user.setPassword(passwordEncoder.encode(password.getNewPassword()));
        usersService.updateUser(user);

        return "redirect:/user/" + username + "/edit/password/confirm";
    }

    @GetMapping("/{username}/edit/password/confirm")
    public String showPasswordChangesPage(@PathVariable String username, HttpServletRequest request,  Model model) {
        User loggedUser = usersService.getUserByUsername(request.getUserPrincipal().getName());
        User user = usersService.getUserByUsername(username);
        model.addAttribute("user", user);
        model.addAttribute("userInfo", user.getUserInfo());
        model.addAttribute("loggedUser", loggedUser);
        model.addAttribute("unread", notificationsService.getAllUnread(user.getUserInfo()));
        return "change-password-confirm";
    }

    @GetMapping("/{username}/edit/interests")
    public String showEditInterests(@PathVariable String username, HttpServletRequest request, Model model) {
        User loggedUser = usersService.getUserByUsername(request.getUserPrincipal().getName());
        User user = usersService.getUserByUsername(username);
        if (loggedUser != user && !request.isUserInRole("ADMIN")) {
            return "access-denied";
        }
        model.addAttribute("user", user);
        model.addAttribute("userInfo", user.getUserInfo());
        model.addAttribute("genres", genresService.getAll());
        model.addAttribute("newGenre", new MovieGenre());
        model.addAttribute("userGenres", user.getUserInfo().getGenres());
        model.addAttribute("loggedUser", loggedUser);
        model.addAttribute("unread", notificationsService.getAllUnread(user.getUserInfo()));
        return "edit-profile-interests";
    }

    @PostMapping("/{username}/edit/interests")
    public String addInterests(@PathVariable String username, @ModelAttribute("newGenre") MovieGenre genre) {
        User user = usersService.getUserByUsername(username);
        UserInfo userInfo = user.getUserInfo();
        userInfo.addGenre(genresService.getByName(genre.getGenre()));
        usersService.updateInfoLink(userInfo, username);
        return "redirect:/user/" + username + "/edit/interests";
    }


    @PostMapping("/{username}/edit/interests/remove")
    public String removeInterests(@PathVariable String username, @RequestParam String genre) {
        User user = usersService.getUserByUsername(username);
        UserInfo userInfo = user.getUserInfo();
        userInfo.removeGenre(genresService.getByName(genre));
        usersService.updateInfoLink(userInfo, username);
        return "redirect:/user/" + username + "/edit/interests";
    }

    @GetMapping("/{username}/edit/movies")
    public String showEditMovies(@PathVariable String username, HttpServletRequest request, Model model) {
        User loggedUser = usersService.getUserByUsername(request.getUserPrincipal().getName());
        User user = usersService.getUserByUsername(username);
        if (loggedUser != user && !request.isUserInRole("ADMIN")) {
            return "access-denied";
        }
        model.addAttribute("user", user);
        model.addAttribute("userInfo", user.getUserInfo());
        model.addAttribute("movies", moviesService.getAllMoviesByUser(user.getUserInfo().getId()));
        model.addAttribute("loggedUser", loggedUser);
        model.addAttribute("unread", notificationsService.getAllUnread(user.getUserInfo()));
        return "edit-profile-movies";
    }

    @PostMapping("/{username}/edit/movies/remove")
    @Transactional
    public String removeMovies(@PathVariable String username, @RequestParam long id) {
        User user = usersService.getUserByUsername(username);
        moviesService.removeFromFavorites(id, user.getUserInfo().getId());
        return "redirect:/user/" + username + "/edit/movies";
    }

    @GetMapping("/{username}/delete")
    public String showDeleteUser(@PathVariable String username, HttpServletRequest request, Model model) {
        User loggedUser = usersService.getUserByUsername(request.getUserPrincipal().getName());
        User user = usersService.getUserByUsername(username);
        if (loggedUser != user && !request.isUserInRole("ADMIN")) {
            return "access-denied";
        }
        model.addAttribute("user", user);
        model.addAttribute("userInfo", user.getUserInfo());
        model.addAttribute("loggedUser", loggedUser);
        model.addAttribute("unread", notificationsService.getAllUnread(user.getUserInfo()));
        return "delete-user";
    }

    @PostMapping("/{username}/delete")
    @Transactional
    public String deleteUser(@PathVariable String username) {
        User userToUpdate = usersService.getUserByUsername(username);
        likesService.deleteLikesByUser(userToUpdate.getUserInfo());
        commentsService.deleteCommentsByUser(userToUpdate.getUserInfo());
        postsService.deletePostsByUser(userToUpdate.getUserInfo());
        friendsService.removeAllFriends(userToUpdate.getUserInfo());
        userToUpdate.setEnabled(false);
        usersService.updateUser(userToUpdate);
        return "redirect:/";
    }

    @GetMapping("/{username}/friends")
    public String showUserFriends(@PathVariable String username, HttpServletRequest request, Model model) {
        User user = usersService.getUserByUsername(username);
        User loggedUser = usersService.getUserByUsername(request.getUserPrincipal().getName());
        List<UserInfo> friends = friendsService.getAllFriends(user.getUserInfo());
        model.addAttribute("user", user);
        model.addAttribute("userInfo", user.getUserInfo());
        model.addAttribute("friends", friends);
        model.addAttribute("friendRequests", friendsService.getAllRequests(loggedUser.getUserInfo()));
        model.addAttribute("loggedUser", loggedUser);
        model.addAttribute("status", friendsService.checkForConnection(loggedUser.getUserInfo(), user.getUserInfo()));
        model.addAttribute("unread", notificationsService.getAllUnread(user.getUserInfo()));
        return "personal-friends";
    }

    @GetMapping("/{username}/notifications")
    public String showUserNotifications(@PathVariable String username, HttpServletRequest request, Model model) {
        User loggedUser = usersService.getUserByUsername(request.getUserPrincipal().getName());
        User user = usersService.getUserByUsername(username);
        model.addAttribute("user", user);
        model.addAttribute("userInfo", user.getUserInfo());
        model.addAttribute("loggedUser", loggedUser);
        model.addAttribute("notifications", notificationsService.getAllByUser(user.getUserInfo()));
        model.addAttribute("unread", notificationsService.getAllUnread(user.getUserInfo()));
        model.addAttribute("dateFormat", df);
        return "personal-notifications";
    }

    @GetMapping("/search")
    public String searchUser(@RequestParam(value = "word") String word, Principal principal, Model model) {
        User user = usersService.getUserByUsername(principal.getName());
        model.addAttribute("user", user);
        model.addAttribute("userInfo", user.getUserInfo());
        model.addAttribute("friends", usersInfoService.getAllByNameAndStatusFriends(user.getUserInfo(), word));
        model.addAttribute("others", usersInfoService.getAllByNameAndOtherStatus(user.getUserInfo(), word));
        model.addAttribute("unread", notificationsService.getAllUnread(user.getUserInfo()));
        return "newsfeed-users";
    }

    @GetMapping("/movies")
    public String searchMovies(Principal principal, Model model) {
        User user = usersService.getUserByUsername(principal.getName());
        model.addAttribute("user", user);
        model.addAttribute("userInfo", user.getUserInfo());
        model.addAttribute("unread", notificationsService.getAllUnread(user.getUserInfo()));
        return "search-movies";
    }

    @GetMapping("/image")
    public void renderUserImage(@RequestParam String username, HttpServletResponse response) throws IOException {
        User user = usersService.getUserByUsername(username);
        if (user.getUserInfo().getPicture() != null) {
            response.setContentType("image/jpeg");
            InputStream is = new ByteArrayInputStream(Base64.getDecoder().decode(user.getUserInfo().getPicture()));
            IOUtils.copy(is, response.getOutputStream());
        }
    }

    @GetMapping("/image/cover")
    public void renderCoverImage(@RequestParam String username, HttpServletResponse response) throws IOException {
        User user = usersService.getUserByUsername(username);
        if (user.getUserInfo().getCoverPicture() != null) {
            response.setContentType("image/jpeg");
            InputStream is = new ByteArrayInputStream(Base64.getDecoder().decode(user.getUserInfo().getCoverPicture()));
            IOUtils.copy(is, response.getOutputStream());
        }
    }

    private static void parsePaginationParameters(int page, int size, HttpServletRequest request) {
        if (request.getParameter("page") != null && !request.getParameter("page").isEmpty()) {
            page = Integer.parseInt(request.getParameter("page")) - 1;
        }

        if (request.getParameter("size") != null && !request.getParameter("size").isEmpty()) {
            size = Integer.parseInt(request.getParameter("size"));
        }
    }

}
