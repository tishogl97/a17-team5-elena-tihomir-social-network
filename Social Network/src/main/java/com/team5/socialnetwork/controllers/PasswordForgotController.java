package com.team5.socialnetwork.controllers;

import com.team5.socialnetwork.models.ConfirmationToken;
import com.team5.socialnetwork.models.User;
import com.team5.socialnetwork.models.UserInfo;
import com.team5.socialnetwork.services.ConfirmationTokenService;
import com.team5.socialnetwork.services.EmailSenderService;
import com.team5.socialnetwork.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping("/forgotten-password")
public class PasswordForgotController {

    private UsersService usersService;
    private ConfirmationTokenService confirmationTokenService;
    private EmailSenderService emailSenderService;

    @Autowired
    public PasswordForgotController(UsersService usersService,
                                    ConfirmationTokenService confirmationTokenService,
                                    EmailSenderService emailSenderService) {
        this.usersService = usersService;
        this.confirmationTokenService = confirmationTokenService;
        this.emailSenderService = emailSenderService;
    }

    @GetMapping
    public String showEnterEmail(Model model) {
        model.addAttribute("userInfo", new UserInfo());
        return "forgotten-password-email";
    }

    @PostMapping
    public String enterEmail(@ModelAttribute UserInfo userInfo, Model model) {
        User user = usersService.getUserByEmail(userInfo.getEmail());
        if (user != null) {
            ConfirmationToken confirmationToken = new ConfirmationToken(user.getUserInfo());
            confirmationTokenService.createToken(confirmationToken);
            confirmationToken.setUser(userInfo);

            SimpleMailMessage mailMessage = new SimpleMailMessage();
            mailMessage.setTo(userInfo.getEmail());
            mailMessage.setSubject("Reset Password.");
            mailMessage.setFrom("elena.veskova@mail.com");
            mailMessage.setText("To reset your password, please click here: "
                    + "http://localhost:8080/reset-password?token=" + confirmationToken.getConfirmationToken());
            emailSenderService.sendEmail(mailMessage);

            model.addAttribute("success", "Check your e-mail to reset password.");
        } else {
            model.addAttribute("error", "User with that e-mail does not exist!");
        }
        return "forgotten-password-email";
    }

}
