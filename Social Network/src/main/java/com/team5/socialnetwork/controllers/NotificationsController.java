package com.team5.socialnetwork.controllers;

import com.team5.socialnetwork.models.Notifications;
import com.team5.socialnetwork.models.User;
import com.team5.socialnetwork.models.UserInfo;
import com.team5.socialnetwork.services.NotificationsService;
import com.team5.socialnetwork.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;

@Controller
@RequestMapping("/notification")
public class NotificationsController {

    private NotificationsService notificationsService;

    @Autowired
    public NotificationsController(NotificationsService notificationsService) {
        this.notificationsService = notificationsService;
    }

    @PostMapping
    public String markAsRead(@RequestParam long id) {
        Notifications notification = notificationsService.getById(id);
        notification.setRead(true);
        notificationsService.saveNotification(notification);
        return "redirect:/feed/post/details?id=" + notification.getPost().getId();
    }

    @PostMapping("/delete")
    @Transactional
    public String deleteNotification(@RequestParam long id) {
        Notifications notification = notificationsService.getById(id);
        notificationsService.deleteNotification(notification);
        return "redirect:/user/" + notification.getUser().getUser().getUsername() + "/notifications";
    }
}
