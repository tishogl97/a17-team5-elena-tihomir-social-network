package com.team5.socialnetwork.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "users_posts")
public class Posts {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "post_id")
    private long id;

    @Column(name = "text")
    @NotBlank
    @NotNull
    @Size(min = 2, max = 1500)
    private String text;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserInfo creator;

    @Column(name = "picture")
    private String picture;

    @Column(name = "privacy")
    private boolean privacy;

    @Column(name = "date_and_time")
    private Timestamp date;

    @OneToMany(mappedBy = "post")
    @JsonIgnore
    private List<Comments> comments;

    @OneToMany(mappedBy = "post")
    @JsonIgnore
    private List<Likes> likes;

    public Posts() {
    }

    public Posts(String text, UserInfo creator) {
        this.text = text;
        this.creator = creator;
    }
    public Posts(int id, String text) {
        this.text = text;
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public UserInfo getCreator() {
        return creator;
    }

    public void setCreator(UserInfo creator) {
        this.creator = creator;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public boolean getPrivacy() {
        return privacy;
    }

    public void setPrivacy(boolean privacy) {
        this.privacy = privacy;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public boolean isPrivacy() {
        return privacy;
    }

    public List<Comments> getComments() {
        return comments;
    }

    public void setComments(List<Comments> comments) {
        this.comments = comments;
    }

    public void addComment(Comments comment) {
        comments.add(comment);
    }

    public List<Likes> getLikes() {
        return likes;
    }

    public void setLikes(List<Likes> likes) {
        this.likes = likes;
    }

}
