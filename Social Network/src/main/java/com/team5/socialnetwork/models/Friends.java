package com.team5.socialnetwork.models;

import javax.persistence.*;

@Entity
@Table(name = "users_friends")
public class Friends {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserInfo requestReceiver;

    @ManyToOne
    @JoinColumn(name = "friend_id")
    private UserInfo requestSender;

    @Column(name = "status")
    private String status;

    public Friends() {
    }

    public Friends(UserInfo requestSender, UserInfo requestReceiver, String status) {
        this.requestSender = requestSender;
        this.requestReceiver = requestReceiver;
        this.status = status;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public UserInfo getRequestReceiver() {
        return requestReceiver;
    }

    public void setRequestReceiver(UserInfo requestReceiver) {
        this.requestReceiver = requestReceiver;
    }

    public UserInfo getRequestSender() {
        return requestSender;
    }

    public void setRequestSender(UserInfo requestSender) {
        this.requestSender = requestSender;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
