package com.team5.socialnetwork.models;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "users")
public class User {

    private static final String USERNAME_MESSAGE = "Username has to be at least 3 characters.";
    private static final String PASSWORD_MESSAGE = "Password has to be at least 3 characters.";

    @Id
    @NotNull
    @NotBlank
    @Size(min = 3, message = USERNAME_MESSAGE)
    @Column(name = "username")
    private String username;

    @NotNull
    @NotBlank
    @Size(min = 3, message = PASSWORD_MESSAGE)
    @Column(name = "password")
    private String password;

    @Size(min = 3, message = PASSWORD_MESSAGE)
    @Transient
    private String passwordConfirmation;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private UserInfo userInfo;

    @Column(name = "enabled")
    private boolean enabled;

    public User() {
    }

    public User(String username, UserInfo userInfo) {
        this.username = username;
        this.userInfo = userInfo;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}

