package com.team5.socialnetwork.models;


import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class PostsDTO {

    @NotNull
    @NotBlank
    @Size(min = 2, max = 1500)
    private String text;

    private boolean privacy;

    public PostsDTO() {
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean getPrivacy() {
        return privacy;
    }

    public void setPrivacy(boolean privacy) {
        this.privacy = privacy;
    }

}
