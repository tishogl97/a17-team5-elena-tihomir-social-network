package com.team5.socialnetwork.models;

import javax.validation.constraints.Size;

public class UserPassword {

    private static final String CURRENT_PASSWORD_MESSAGE = "Current password has to be at least 3 characters.";
    private static final String NEW_PASSWORD_MESSAGE = "New password has to be at least 3 characters.";
    private static final String REPEAT_NEW_PASSWORD_MESSAGE = "Repeated password has to be at least 3 characters.";

    @Size(min = 3, message = CURRENT_PASSWORD_MESSAGE)
    private String currentPassword;

    @Size(min = 3, message = NEW_PASSWORD_MESSAGE)
    private String newPassword;

    @Size(min = 3, message = REPEAT_NEW_PASSWORD_MESSAGE)
    private String repeatNewPassword;

    public UserPassword() {
    }

    public String getCurrentPassword() {
        return currentPassword;
    }

    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getRepeatNewPassword() {
        return repeatNewPassword;
    }

    public void setRepeatNewPassword(String repeatNewPassword) {
        this.repeatNewPassword = repeatNewPassword;
    }

}
