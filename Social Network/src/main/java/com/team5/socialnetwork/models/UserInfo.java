package com.team5.socialnetwork.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Table(name = "users_info")
public class UserInfo {

    private static final String FIRST_NAME_MESSAGE = "First name has to be at least 2 characters.";
    private static final String LAST_NAME_MESSAGE = "Last has to be at least 2 characters.";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private int id;

    @JsonIgnore
    @OneToOne(mappedBy = "userInfo")
    private User user;

    @NotNull
    @NotBlank
    @Size(min = 2, message = FIRST_NAME_MESSAGE)
    @Column(name = "first_name")
    private String firstName;

    @NotNull
    @NotBlank
    @Size(min = 2, message = LAST_NAME_MESSAGE)
    @Column(name = "last_name")
    private String lastName;

    @NotNull
    @NotBlank
    @Email
    @Column(name = "email")
    private String email;

    @Column(name = "email_privacy")
    private boolean emailPrivacy;

    @Column(name = "picture")
    private String picture;

    @Column(name = "picture_privacy")
    private boolean picturePrivacy;

    @Column(name = "cover_picture")
    private String coverPicture;

    @Column(name = "gender")
    private String gender;

    @Column(name = "gender_privacy")
    private boolean genderPrivacy;

    @Transient
    private String day;

    @Transient
    private String month;

    @Transient
    private String year;

    @Column(name = "birthday")
    private String birthday;

    @Column(name = "birthday_privacy")
    private boolean datePrivacy;

    @Column(name = "description")
    private String description;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "work_id")
    private UserWork userWork;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id")
    private UserAddress address;

    @OneToOne(mappedBy = "user", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private ConfirmationToken token;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "users_genres",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "genre_id"))
    private Set<MovieGenre> genres;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "users_movies",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "movie_id"))
    private Set<Movie> movies;

    public UserInfo() {
    }

    public UserInfo(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isEmailPrivacy() {
        return emailPrivacy;
    }

    public void setEmailPrivacy(boolean emailPrivacy) {
        this.emailPrivacy = emailPrivacy;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public boolean isPicturePrivacy() {
        return picturePrivacy;
    }

    public void setPicturePrivacy(boolean picturePrivacy) {
        this.picturePrivacy = picturePrivacy;
    }

    public String getCoverPicture() {
        return coverPicture;
    }

    public void setCoverPicture(String coverPicture) {
        this.coverPicture = coverPicture;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public boolean isGenderPrivacy() {
        return genderPrivacy;
    }

    public void setGenderPrivacy(boolean genderPrivacy) {
        this.genderPrivacy = genderPrivacy;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public boolean isDatePrivacy() {
        return datePrivacy;
    }

    public void setDatePrivacy(boolean datePrivacy) {
        this.datePrivacy = datePrivacy;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public UserWork getUserWork() {
        return userWork;
    }

    public void setUserWork(UserWork userWork) {
        this.userWork = userWork;
    }

    public UserAddress getAddress() {
        return address;
    }

    public void setAddress(UserAddress address) {
        this.address = address;
    }

    public ConfirmationToken getToken() {
        return token;
    }

    public void setToken(ConfirmationToken token) {
        this.token = token;
    }

    public Set<MovieGenre> getGenres() {
        return genres;
    }

    public void setGenres(Set<MovieGenre> genres) {
        this.genres = genres;
    }

    public void addGenre(MovieGenre genre) {
        genres.add(genre);
    }

    public void removeGenre(MovieGenre genre) {
        genres.remove(genre);
    }

    public Set<Movie> getMovies() {
        return movies;
    }

    public void setMovies(Set<Movie> movies) {
        this.movies = movies;
    }

    public void addMovies(Movie movie) {
        movies.add(movie);
    }
}

