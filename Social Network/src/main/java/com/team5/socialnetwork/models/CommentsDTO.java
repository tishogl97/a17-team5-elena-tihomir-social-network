package com.team5.socialnetwork.models;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CommentsDTO {

    @NotNull
    @NotBlank
    @Size(min = 2, max = 500)
    private String text;

    public CommentsDTO() {
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
