package com.team5.socialnetwork.utils;

import com.team5.socialnetwork.models.User;
import com.team5.socialnetwork.models.UserInfo;
import com.team5.socialnetwork.models.UserWork;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityNotFoundException;
import java.io.IOException;
import java.util.Base64;

import static com.team5.socialnetwork.constants.LoggerConstants.THROW_WHEN_POST_DOES_NOT_EXIST;
import static com.team5.socialnetwork.constants.LoggerConstants.THROW_WHEN_USER_DOES_NOT_EXIST;

public class UsersUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(UsersUtils.class.getName());

    public static UserInfo mapUserInfo(User user, UserInfo userInfo) {
        UserInfo userInfoToUpdate = user.getUserInfo();
        userInfoToUpdate.setFirstName(userInfo.getFirstName());
        userInfoToUpdate.setLastName(userInfo.getLastName());
        userInfoToUpdate.setEmail(userInfo.getEmail());
        userInfoToUpdate.setEmailPrivacy(userInfo.isEmailPrivacy());
        String day = userInfo.getDay();
        String month = userInfo.getMonth();
        String year = userInfo.getYear();
        userInfoToUpdate.setBirthday(day + "-" + month + "-" + year);
        userInfoToUpdate.setDatePrivacy(userInfo.isDatePrivacy());
        userInfoToUpdate.setGender(userInfo.getGender());
        userInfoToUpdate.setGenderPrivacy(userInfo.isGenderPrivacy());
        userInfoToUpdate.getAddress().setTown(userInfo.getAddress().getTown());
        userInfoToUpdate.getAddress().setCountry(userInfo.getAddress().getCountry());
        userInfoToUpdate.getAddress().setPrivacy(userInfo.getAddress().isPrivacy());
        userInfoToUpdate.setDescription(userInfo.getDescription());
        return userInfoToUpdate;
    }

    public static UserWork mapUserWork(User user, UserWork userWork) {
        UserWork userWorkToUpdate = user.getUserInfo().getUserWork();
        userWorkToUpdate.setCompany(userWork.getCompany());
        userWorkToUpdate.setPosition(userWork.getPosition());
        userWorkToUpdate.setFrom(userWork.getFrom());
        userWorkToUpdate.setTo(userWork.getTo());
        userWorkToUpdate.setDescription(userWork.getDescription());
        userWorkToUpdate.setEnabled(userWork.isEnabled());
        userWorkToUpdate.setPrivacy(userWork.isPrivacy());
        return userWorkToUpdate;
    }

    public static void checkIfUserExists(UserInfo user) {
        if (user == null) {
            LOGGER.error(THROW_WHEN_USER_DOES_NOT_EXIST);
            throw new EntityNotFoundException("The user doesn't exist!");
        }
    }
}
