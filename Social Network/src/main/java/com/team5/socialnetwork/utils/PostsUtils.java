package com.team5.socialnetwork.utils;

import com.team5.socialnetwork.exceptions.NoPermissionException;
import com.team5.socialnetwork.models.Posts;
import com.team5.socialnetwork.models.UserInfo;
import com.team5.socialnetwork.repositories.PostsRepository;
import com.team5.socialnetwork.services.PostsServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityNotFoundException;
import java.util.List;

import static com.team5.socialnetwork.constants.LoggerConstants.DELETE_POST_MESSAGE;
import static com.team5.socialnetwork.constants.LoggerConstants.THROW_WHEN_POST_DOES_NOT_EXIST;

public class PostsUtils {

    private static final String POST_NOT_FOUND = "Post was not found!.";
    private static final String NOT_PERMITTED = "You do not have permission to do it!";

    private static final Logger LOGGER = LoggerFactory.getLogger(PostsUtils.class.getName());

    public static void checkIfPostExists(Posts post) {
        if(post == null) {
            LOGGER.error(THROW_WHEN_POST_DOES_NOT_EXIST);
            throw new EntityNotFoundException(POST_NOT_FOUND);
        }
    }

    public static void  checkPermission(UserInfo creator, UserInfo user) {
        if(creator != user) {
            throw new NoPermissionException(NOT_PERMITTED);
        }
    }

    public static void  checkPermission(UserInfo creator, UserInfo user, boolean isAdmin) {
        if(creator != user && !isAdmin) {
            throw new NoPermissionException(NOT_PERMITTED);
        }
    }
}
