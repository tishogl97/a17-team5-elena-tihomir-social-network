package com.team5.socialnetwork.utils;

import com.team5.socialnetwork.models.Friends;
import com.team5.socialnetwork.models.UserInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityNotFoundException;

import static com.team5.socialnetwork.constants.LoggerConstants.THROW_WHEN_POST_DOES_NOT_EXIST;
import static com.team5.socialnetwork.constants.LoggerConstants.THROW_WHEN_REQUEST_DOES_NOT_EXIST;

public class FriendsUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(FriendsUtils.class.getName());

    public static void checkIfRequestExists(Friends request) {
        if (request == null) {
            LOGGER.error(THROW_WHEN_REQUEST_DOES_NOT_EXIST);
            throw new EntityNotFoundException(THROW_WHEN_REQUEST_DOES_NOT_EXIST);
        }
    }

    public static boolean checkIfAlreadyConnected(Friends friends) {
        if (friends == null) {
            return false;
        }
        return true;
    }

}
