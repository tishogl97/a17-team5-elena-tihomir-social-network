package com.team5.socialnetwork.utils;

import com.team5.socialnetwork.models.Comments;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityNotFoundException;

import static com.team5.socialnetwork.constants.LoggerConstants.THROW_WHEN_COMMENT_DOES_NOT_EXIST;
import static com.team5.socialnetwork.constants.LoggerConstants.THROW_WHEN_POST_DOES_NOT_EXIST;

public class CommentsUtils {

    private static final String COMMENT_NOT_FOUND = "The comment you are looking for doesn't exist!";

    private static final Logger LOGGER = LoggerFactory.getLogger(CommentsUtils.class.getName());

    public static void checkIfCommentExists(Comments comment) {
        if(comment == null) {
            LOGGER.error(THROW_WHEN_COMMENT_DOES_NOT_EXIST);
            throw new EntityNotFoundException(COMMENT_NOT_FOUND);
        }
    }
}
