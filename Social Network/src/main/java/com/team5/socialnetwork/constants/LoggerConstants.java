package com.team5.socialnetwork.constants;

public class LoggerConstants {

    public static final String CREATE_COMMENT_MESSAGE = "Comment with ID %d was created.";
    public static final String EDIT_COMMENT_MESSAGE = "Comment with ID %d was edited.";
    public static final String FIND_COMMENT_BY_ID_MESSAGE = "Get comment with ID %d.";
    public static final String FIND_COMMENT_BY_POST_MESSAGE = "Get comment with post with ID %d.";
    public static final String DELETE_COMMENTS_BY_POST_MESSAGE = "Delete all comments on post with post ID %d.";
    public static final String DELETE_ALL_COMMENTS_BY_USER_MESSAGE = "Delete all comments by %s %s.";
    public static final String DELETE_COMMENT_MESSAGE = "Delete comment with ID %d.";

    public static final String FRIEND_REQUEST_SENT_MESSAGE = "Friend request with ID %d was sent to %s %s.";
    public static final String FIND_ALL_REQUESTS_TO_USER_MESSAGE = "Get all friend requests to %s %s.";
    public static final String FIND_ALL_REQUESTS_AND_SENDS_OF_USER_MESSAGE = "Get all friend requests and sends to %s %s.";
    public static final String FIND_ALL_FRIENDS_OF_USER_MESSAGE = "Get all friends of %s %s.";
    public static final String FIND_REQUEST_BY_ID_MESSAGE = "Get request with ID %d.";
    public static final String ACCEPT_REQUEST_MESSAGE = "Request with ID %d accepted.";
    public static final String DECLINE_REQUEST_MESSAGE = "Request with ID %d declined.";
    public static final String DELETE_FRIENDSHIP_MESSAGE = "Delete friendship with ID %d.";
    public static final String DELETE_ALL_FRIENDSHIPS_OF_USER_MESSAGE = "Delete all friendships of %s %s.";

    public static final String CREATE_LIKE_MESSAGE = "Like with ID %d was created.";
    public static final String DISLIKE_MESSAGE = "Like with ID %d was deleted.";
    public static final String DELETE_LIKES_BY_POST_MESSAGE = "Delete all likes on post with post ID %d.";
    public static final String DELETE_ALL_LIKES_BY_USER_MESSAGE = "Delete all likes by %s %s.";
    public static final String FIND_LIKE_BY_ID_MESSAGE = "Get like by post ID %d.";

    public static final String CREATE_NOTIFICATION_MESSAGE = "Notification with ID %d was created.";
    public static final String DELETE_NOTIFICATION_MESSAGE = "Notification with ID %d was deleted.";
    public static final String FIND_NOTIFICATION_BY_ID_MESSAGE = "Get notification by post ID %d.";
    public static final String FIND_ALL_NOTIFICATIONS_OF_USER_MESSAGE = "Get all notifications of %s %s.";
    public static final String FIND_ALL_UNREAD_NOTIFICATIONS_OF_USER_MESSAGE = "Get all unread notifications of %s %s.";

    public static final String CREATE_POST_MESSAGE = "Post with ID %d was created.";
    public static final String DELETE_POST_MESSAGE = "Post with ID %d was deleted.";
    public static final String EDIT_POST_MESSAGE = "Post with ID %d was edited.";
    public static final String DELETE_ALL_POSTS_BY_USER_MESSAGE = "All posts created by %s %s were deleted.";
    public static final String SHOW_PERSONAL_FEED_MESSAGE = "%s %s feed was shown.";
    public static final String FIND_POST_BY_ID_MESSAGE = "Get post with ID %d.";
    public static final String GET_ALL_POSTS_BY_USER_MESSAGE = "Get all posts created by %s %s friends.";

    public static final String SEARCH_USERS_BY_NAME_MESSAGE = "Get all users with name like %s.";
    public static final String SEARCH_USERS_WITH_STATUS_FRIENDS_BY_NAME_MESSAGE = "Get all user's friends with name like %s.";
    public static final String SEARCH_USERS_WITH_OTHER_STATUS_BY_NAME_MESSAGE = "Get all users with other status type with name like %s.";
    public static final String FIND_FRIENDS_BY_ID_MESSAGE = "Get friends with ID %d.";

    public static final String FIND_ALL_ENABLED_USERS_MESSAGE = "Get all enabled users.";
    public static final String FIND_USER_BY_USERNAME_MESSAGE = "Get user by username %s.";
    public static final String FIND_USER_BY_EMAIL_MESSAGE = "Get user by email %s.";
    public static final String CREATE_USER_MESSAGE = "User with ID %d was created.";
    public static final String CREATE_WORK_MESSAGE = "Work with ID %d was created.";
    public static final String UPDATE_INFO_LINK_MESSAGE = "Information about user with ID %d was updated.";
    public static final String UPDATE_WORK_LINK_MESSAGE = "Work of user with ID %d was updated.";
    public static final String UPDATE_USER_MESSAGE = "User with ID %d was updated.";

    public static final String THROW_WHEN_POST_DOES_NOT_EXIST = "Post does not exist!";
    public static final String THROW_WHEN_COMMENT_DOES_NOT_EXIST = "Comment does not exist!";
    public static final String THROW_WHEN_USERS_ALREADY_CONNECTED = "Users are already connected!";
    public static final String THROW_WHEN_REQUEST_DOES_NOT_EXIST = "Request does not exist!";
    public static final String THROW_WHEN_USER_DOES_NOT_EXIST = "User does not exist!";
    public static final String THROW_WHEN_EMAIL_ALREADY_EXISTS = "User with this e-mail already exists!";

}
