package com.team5.socialnetwork;

import com.team5.socialnetwork.services.PostsServiceImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.util.logging.Logger;

@EnableJpaRepositories(basePackages = "com.team5.socialnetwork.repositories")
@SpringBootApplication
public class SocialnetworkApplication {

    private static final Logger LOGGER = Logger.getLogger(SocialnetworkApplication.class.getName());

    public static void main(String[] args) {
        LOGGER.info("--- Application started. ---");
        SpringApplication.run(SocialnetworkApplication.class, args);
    }

}
