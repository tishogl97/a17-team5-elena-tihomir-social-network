# A17-Team5-Tihomir-Elena-Social-Network

**Project: ConnectFlix - Cinema Social Network**

***Purpose:***

*Web application which allows users to connect with friends, or people that they don't know. It allows them to share pictures,
articles or just thoughts about everything in the world of cinema and receive reactions from others in the form of comments or just likes.
Every user can customize their own profile they way they want. Any information can be included and made visible or hidden for people they don't know.
They can point which genres they like and also search for movies and add them to favourites.*


***Creators:***
Elena & Tihomir

***Functionalities:***


* **Create profile and join the community**

![](pictures/Registration.png)

**Or just log in, if you have already existing account**
        
![](pictures/logIn.png)


*  **Fill in information about yourself or change the already existing one.**

![](pictures/OveralEditTab.png)


*  **Search people. You might find some of your friends or just make new ones and see what they think about anything**
**in the world of cinema.**
![](pictures/loggedHome.png)

*  **Search movies**

![](pictures/searchMovies.png)

**And you can add them to favourites**
    
![](pictures/addFavouriteMovies.png)


***Resources:***
*  Spring
*  Spring Security
*  Spring MVC
*  Thymeleaf
*  JPA
*  jQuery, Ajax
*  Mockito
*  Gradle

***To run the project:***
1.  You need Java 8+, JDK, IntelliJ
2.  Clone the project on your PC/Laptop
3.  Create the database with the script create_db.sql inside "db" folder
4.  Insert the date with the script insert_data.sql inside "db" folder


*Telerik Academy: Social Network Teamwork Project - Tihomir & Elena*

Trello Link: https://trello.com/b/W5cZU7Mf/social-network-teamwork

